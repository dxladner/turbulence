<?php 

class Text_Editor_Custom_Control extends WP_Customize_Control
{
    public $type = 'textarea';
    /**
    ** Render the content on the theme customizer page
    */
    public function render_content() 
    { ?>
        <label>
          <span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
          <?php
            $settings = array(
              'media_buttons' => false,
              'quicktags' => false,
               'tinymce'	=> true,
              );
            wp_editor($this->value(), $this->id, $settings );
          ?>
        </label>
        <?php
	}
}
