<?php 
/*========= Blog Settings Section =========*/
    // blog settings section
    Kirki::add_section( 'turbulence_blog_section', array(
        'priority'          => 10,
        'title'          => esc_attr__( 'Blog Settings', 'turbulence' ),
        'priority'       => 1,
        'capability'     => 'edit_theme_options',
    ) );
    // blog display meta data enable control
    Kirki::add_field( 'turbulence_display_meta_data', array(
        'type'        => 'switch',
        'settings'    => 'turbulence_display_meta_data',
        'label'       => esc_attr__( 'Display Meta Data', 'turbulence' ),
        'description' => esc_attr__( 'Select to enable/disable the date, author, categories and comments number.', 'turbulence' ),
        'section'     => 'turbulence_blog_section',
        'default'     => true,
        'priority'    => 10,
        'required'    => array(
            array(
                'operator' => '==',
                'value'    => true,
            ),
        ),
    ) );
    // read more button text control
    Kirki::add_field( 'turbulence_read_more_button_text', array(
        'type'        => 'text',
        'settings'    => 'turbulence_read_more_button_text',
        'label'       => esc_attr__( 'Read More Button Text', 'turbulence' ),
        'description' => esc_attr__( 'This is the text that will replace Read More.', 'turbulence' ),
        'section'     => 'turbulence_blog_section',
        'default'     => '',
        'priority'    => 10,
    ) );
    // blog read more button layout style control
    Kirki::add_field( 'turbulence_read_more_button_full_width_block', array(
        'type'        => 'switch',
        'settings'    => 'turbulence_read_more_button_full_width_block',
        'label'       => esc_attr__( 'Make the Read More button Full Width - Block', 'turbulence' ),
        'description' => esc_attr__( 'Enable/Disable full width button.', 'turbulence' ),
        'section'     => 'turbulence_blog_section',
        'default'     => true,
        'priority'    => 10,
        'required'    => array(
            array(
                'operator' => '==',
                'value'    => true,
            ),
        ),
    ) );
    // blog read more button size control
    Kirki::add_field( 'turbulence_read_more_button_size', array(
        'type'        => 'select',
        'settings'    => 'turbulence_read_more_button_size',
        'label'       => esc_attr__( 'Read More Button Size', 'turbulence' ),
        'description' => esc_attr__( 'Select the Bootstrap button size you want.', 'turbulence' ),
        'section'     => 'turbulence_blog_section',
        'default'     => 'default',
        'priority'    => 10,
        'choices'     => array(
            'xs'            => esc_attr__( 'Extra Small', 'turbulence' ),
            'sm'            => esc_attr__( 'Small', 'turbulence' ),
            'default'       => esc_attr__( 'Medium', 'turbulence' ),
            'lg'            => esc_attr__( 'Large', 'turbulence' ),
        ),
    ) );
    // blog read more button color control
    Kirki::add_field( 'homepage_call_to_action_button_color', array(
        'type'        => 'select',
        'settings'    => 'homepage_call_to_action_button_color',
        'label'       => esc_attr__( 'Read More Button Color', 'turbulence' ),
        'description' => esc_attr__( 'Select the Bootstrap button color you want.', 'turbulence' ),
        'section'     => 'turbulence_blog_section',
        'default'     => 'default',
        'priority'    => 10,
        'choices'     => array(
            'default'           => esc_attr__( 'Default', 'turbulence' ),
            'primary'           => esc_attr__( 'Primary', 'turbulence' ),
            'info'              => esc_attr__( 'Info', 'turbulence' ),
            'success'           => esc_attr__( 'Success', 'turbulence' ),
            'warning'           => esc_attr__( 'Warning', 'turbulence' ),
            'danger'            => esc_attr__( 'Danger', 'turbulence' ),
            'link'              => esc_attr__( 'Link', 'turbulence' ),
        ),
    ) );
    // blog read more button layout style control
    Kirki::add_field( 'turbulence_enable_post_tags', array(
        'type'        => 'switch',
        'settings'    => 'turbulence_enable_post_tags',
        'label'       => esc_attr__( 'Display Tags', 'turbulence' ),
        'description' => esc_attr__( 'Select to enable/disable the post tags.', 'turbulence' ),
        'section'     => 'turbulence_blog_section',
        'default'     => true,
        'priority'    => 10,
        'required'    => array(
            array(
                'operator' => '==',
                'value'    => true,
            ),
        ),
    ) );