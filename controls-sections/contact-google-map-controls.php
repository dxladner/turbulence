<?php 
	$state_names = array(
        'Alabama'           => esc_attr__( 'Alabama', 'turbulence' ),
        'Alaska'            => esc_attr__( 'Alaska', 'turbulence' ),
        'Arizona'           => esc_attr__( 'Arizona', 'turbulence' ),
        'Arkansas'          => esc_attr__( 'Arkansas', 'turbulence' ),
        'California'        => esc_attr__( 'California', 'turbulence' ),
        'Colorado'          => esc_attr__( 'Colorado', 'turbulence' ),
        'Connecticut'       => esc_attr__( 'Connecticut', 'turbulence' ),
        'Delaware'          => esc_attr__( 'Delaware', 'turbulence' ),
        'Florida'           => esc_attr__( 'Florida', 'turbulence' ),
        'Georgia'           => esc_attr__( 'Georgia', 'turbulence' ),
        'Hawaii'            => esc_attr__( 'Hawaii', 'turbulence' ),
        'Idaho'             => esc_attr__( 'Idaho', 'turbulence' ),
        'Illinois'          => esc_attr__( 'Illinois', 'turbulence' ),
        'Indiana'           => esc_attr__( 'Indiana', 'turbulence' ),
        'Iowa'              => esc_attr__( 'Iowa', 'turbulence' ),
        'Kansas'            => esc_attr__( 'Kansas', 'turbulence' ),
        'Kentucky'          => esc_attr__( 'Kentucky', 'turbulence' ),
        'Louisiana'         => esc_attr__( 'Louisiana', 'turbulence' ),
        'Maine'             => esc_attr__( 'Maine', 'turbulence' ),
        'Maryland'          => esc_attr__( 'Maryland', 'turbulence' ),
        'Massachusetts'     => esc_attr__( 'Massachusetts', 'turbulence' ),
        'Michigan'          => esc_attr__( 'Michigan', 'turbulence' ),
        'Minnesota'         => esc_attr__( 'Minnesota', 'turbulence' ),
        'Mississippi'       => esc_attr__( 'Mississippi', 'turbulence' ),
        'Missouri'          => esc_attr__( 'Missouri', 'turbulence' ),
        'Montana'           => esc_attr__( 'Montana', 'turbulence' ),
        'Nebraska'          => esc_attr__( 'Nebraska', 'turbulence' ),
        'Nevada'            => esc_attr__( 'Nevada', 'turbulence' ),
        'New Hampshire'     => esc_attr__( 'New Hampshire', 'turbulence' ),
        'New Jersey'        => esc_attr__( 'New Jersey', 'turbulence' ),
        'New Mexico'        => esc_attr__( 'New Mexico', 'turbulence' ),
        'New York'          => esc_attr__( 'New York', 'turbulence' ),
        'North Carolina'    => esc_attr__( 'North Carolina', 'turbulence' ),
        'North Dakota'      => esc_attr__( 'North Dakota', 'turbulence' ),
        'Ohio'              => esc_attr__( 'Ohio', 'turbulence' ),
        'Oklahoma'          => esc_attr__( 'Oklahoma', 'turbulence' ),
        'Oregon'            => esc_attr__( 'Oregon', 'turbulence' ),
        'Pennsylvania'      => esc_attr__( 'Pennsylvania', 'turbulence' ),
        'Rhode Island'      => esc_attr__( 'Rhode Island', 'turbulence' ),
        'South Carolina'    => esc_attr__( 'South Carolina', 'turbulence' ),
        'South Dakota'      => esc_attr__( 'South Dakota', 'turbulence' ),
        'Tennessee'         => esc_attr__( 'Tennessee', 'turbulence' ),
        'Texas'             => esc_attr__( 'Texas', 'turbulence' ),
        'Utah'              => esc_attr__( 'Utah', 'turbulence' ),
        'Vermont'           => esc_attr__( 'Vermont', 'turbulence' ),
        'Virginia'          => esc_attr__( 'Virginia', 'turbulence' ),
        'Washington'        => esc_attr__( 'Washington', 'turbulence' ),
        'West Virginia'     => esc_attr__( 'West Virginia', 'turbulence' ),
        'Wisconsin'         => esc_attr__( 'Wisconsin', 'turbulence' ),
        'Wyoming'           => esc_attr__( 'Wyoming', 'turbulence' ),
    );
	$zoom_number = array(
        '1'         => esc_attr__( '1', 'turbulence' ),
        '2'         => esc_attr__( '2', 'turbulence' ),
        '3'         => esc_attr__( '3', 'turbulence' ),
        '4'         => esc_attr__( '4', 'turbulence' ),
        '5'         => esc_attr__( '5', 'turbulence' ),
        '6'         => esc_attr__( '6', 'turbulence' ),
        '7'         => esc_attr__( '7', 'turbulence' ),
        '8'         => esc_attr__( '8', 'turbulence' ),
        '9'         => esc_attr__( '9', 'turbulence' ),
        '10'        => esc_attr__( '10', 'turbulence' ),
        '11'        => esc_attr__( '11', 'turbulence' ),
        '12'        => esc_attr__( '12', 'turbulence' ),
        '13'        => esc_attr__( '13', 'turbulence' ),
        '14'        => esc_attr__( '14', 'turbulence' ),
        '15'        => esc_attr__( '15', 'turbulence' ),
        '16'        => esc_attr__( '16', 'turbulence' ),
        '17'        => esc_attr__( '17', 'turbulence' ),
        '18'        => esc_attr__( '18', 'turbulence' ),
        '19'        => esc_attr__( '19', 'turbulence' ),
        '20'        => esc_attr__( '20', 'turbulence' ),
        
    );

/*========= Contact Google Map Section =========*/
 // google maps section
    Kirki::add_section( 'google_maps', array(
        'priority'       => 10,
        'title'          => esc_attr__( 'Contact-Google Maps', 'turbulence' ),
        'priority'       => 1,
        'capability'     => 'edit_theme_options',
    ) );
    // contact google maps enable control
    Kirki::add_field( 'contact_google_maps_enable', array(
        'type'        => 'switch',
        'settings'    => 'contact_google_maps_enable',
        'label'       => esc_attr__( 'Contact Page/Display Google Maps', 'turbulence' ),
        'description' => esc_attr__( 'Display Google Maps and Business Info on Contact Page.', 'turbulence' ),
        'section'     => 'google_maps',
        'default'     => true,
        'priority'    => 10,
        'required'    => array(
            array(
                'operator' => '==',
                'value'    => true,
            ),
        ),
    ) );
    // google maps business name control
    Kirki::add_field( 'google_maps_business_name', array(
        'type'          => 'text',
        'settings'      => 'google_maps_business_name',
        'label'         => esc_attr__( 'Business Name', 'turbulence' ),
        'description'   => esc_attr__( 'Name of Business', 'turbulence' ),
        'section'       => 'google_maps',
        'default'       => '',
        'priority'      => 10,
    ) );
    // google maps address control
    Kirki::add_field( 'google_maps_address', array(
        'type'        => 'text',
        'settings'    => 'google_maps_address',
        'label'       => esc_attr__( 'Address', 'turbulence' ),
        'description' => esc_attr__( 'Enter Street and Number Address', 'turbulence' ),
        'section'     => 'google_maps',
        'default'     => '',
        'priority'    => 10,
    ) );
    // google maps city control
    Kirki::add_field( 'google_maps_city', array(
        'type'        => 'text',
        'settings'    => 'google_maps_city',
        'label'       => esc_attr__( 'City Name', 'turbulence' ),
        'description' => esc_attr__( 'Name of City', 'turbulence' ),
        'section'     => 'google_maps',
        'default'     => '',
        'priority'    => 10,
    ) );
    // google maps state control
    Kirki::add_field( 'google_maps_state', array(
        'type'        => 'select',
        'settings'    => 'google_maps_state',
        'label'       => esc_attr__( 'State Name', 'turbulence' ),
        'description' => esc_attr__( 'Name of State', 'turbulence' ),
        'section'     => 'google_maps',
        'default'     => 'default',
        'priority'    => 10,
        'choices'     => $state_names,
    ) );
    // google maps zip control
    Kirki::add_field( 'google_maps_zip', array(
        'type'        => 'text',
        'settings'    => 'google_maps_zip',
        'label'       => esc_attr__( 'Zip', 'turbulence' ),
        'description' => esc_attr__( 'Zip Code Number', 'turbulence' ),
        'section'     => 'google_maps',
        'default'     => '',
        'priority'    => 10,
    ) );
    // google maps phone number control
    Kirki::add_field( 'google_maps_phone_number', array(
        'type'        => 'text',
        'settings'    => 'google_maps_phone_number',
        'label'       => esc_attr__( 'Phone', 'turbulence' ),
        'description' => esc_attr__( 'Phone Number', 'turbulence' ),
        'section'     => 'google_maps',
        'default'     => '',
        'priority'    => 10,
    ) );
    // google maps zoom control
    Kirki::add_field( 'google_maps_zoom', array(
        'type'        => 'select',
        'settings'    => 'google_maps_zoom',
        'label'       => esc_attr__( 'Zoom Number', 'turbulence' ),
        'description' => esc_attr__( 'Select the zoom number for your Google Map.', 'turbulence' ),
        'section'     => 'google_maps',
        'default'     => 'default',
        'priority'    => 10,
        'choices'     => $zoom_number,
    ) );
    // google maps contact email control
    Kirki::add_field( 'google_maps_contact_email', array(
        'type'        => 'text',
        'settings'    => 'google_maps_contact_email',
        'label'       => esc_attr__( 'Contact Email', 'turbulence' ),
        'description' => esc_attr__( 'Enter the email address of where you would like the form to go.', 'turbulence' ),
        'section'     => 'google_maps',
        'default'     => '',
        'priority'    => 10,
    ) );