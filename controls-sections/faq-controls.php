<?php 

/*========= FAQ Section =========*/
    // Create custom panel for FAQ Section
    Kirki::add_panel( 'faq', array(
        'priority'    => 10,
        'title'       => __( 'FAQ', 'turbulence' ),
        'description' => __( 'FAQ Settings', 'turbulence' ),
    ) );

/*========= FAQ / Header Section =========*/     
    // faq header section
    Kirki::add_section( 'faq_header_section', array(
        'priority'          => 10,
        'title'          => esc_attr__( 'FAQ Header Section', 'turbulence' ),
        'priority'       => 1,
        'capability'     => 'edit_theme_options',
        'panel'             => 'faq',
    ) );
    // faq header control
    Kirki::add_field( 'faq_header', array(
        'type'          => 'text',
        'settings'      => 'faq_header',
        'label'         => esc_attr__( 'FAQ Header', 'turbulence' ),
        'description'   => esc_attr__( 'Enter FAQ Header', 'turbulence' ),
        'section'       => 'faq_header_section',
        'default'       => '',
        'priority'      => 10,
    ) );
    // faq header paragraph control
    Kirki::add_field( 'faq_paragraph', array(
        'type'        => 'textarea',
        'settings'    => 'faq_paragraph',
        'label'       => esc_attr__( 'FAQ Paragraph', 'turbulence' ),
        'description' => esc_attr__( 'Enter FAQ Pargraph Header', 'turbulence' ),
        'section'     => 'faq_header_section',
        'default'     => '',
        'priority'    => 10,
    ) );

/*========= Question / Answer One =========*/
    // faq one section
    Kirki::add_section( 'faq_one_section', array(
        'priority'          => 10,
        'title'          => esc_attr__( 'FAQ One Section', 'turbulence' ),
        'priority'       => 1,
        'capability'     => 'edit_theme_options',
        'panel'             => 'faq',
    ) );
    // faq question one control
    Kirki::add_field( 'faq_question_one', array(
        'type'          => 'text',
        'settings'      => 'faq_question_one',
        'label'         => esc_attr__( 'FAQ Question One', 'turbulence' ),
        'description'   => esc_attr__( 'Enter Question One', 'turbulence' ),
        'section'       => 'faq_one_section',
        'default'       => '',
        'priority'      => 10,
    ) );
    // faq answer one control
    Kirki::add_field( 'faq_answer_one', array(
        'type'        => 'textarea',
        'settings'    => 'faq_answer_one',
        'label'       => esc_attr__( 'FAQ Answer One', 'turbulence' ),
        'description' => esc_attr__( 'Enter your Answer for Question One', 'turbulence' ),
        'section'     => 'faq_one_section',
        'default'     => '',
        'priority'    => 10,
    ) );
        
/*========= Question / Answer Two =========*/
    // faq two section
    Kirki::add_section( 'faq_two_section', array(
        'priority'          => 10,
        'title'          => esc_attr__( 'FAQ Two Section', 'turbulence' ),
        'priority'       => 1,
        'capability'     => 'edit_theme_options',
        'panel'             => 'faq',
    ) );
    // faq question two control
    Kirki::add_field( 'faq_question_two', array(
        'type'          => 'text',
        'settings'      => 'faq_question_two',
        'label'         => esc_attr__( 'FAQ Question Two', 'turbulence' ),
        'description'   => esc_attr__( 'Enter Question Two', 'turbulence' ),
        'section'       => 'faq_two_section',
        'default'       => '',
        'priority'      => 10,
    ) );
    // faq answer two control
    Kirki::add_field( 'faq_answer_two', array(
        'type'        => 'textarea',
        'settings'    => 'faq_answer_two',
        'label'       => esc_attr__( 'FAQ Answer Two', 'turbulence' ),
        'description' => esc_attr__( 'Enter your Answer for Question Two', 'turbulence' ),
        'section'     => 'faq_two_section',
        'default'     => '',
        'priority'    => 10,
    ) );

/*========= Question / Answer Three =========*/
    // faq three section
    Kirki::add_section( 'faq_three_section', array(
        'priority'          => 10,
        'title'          => esc_attr__( 'FAQ Three Section', 'turbulence' ),
        'priority'       => 1,
        'capability'     => 'edit_theme_options',
        'panel'             => 'faq',
    ) );
    // faq question three control
    Kirki::add_field( 'faq_question_three', array(
        'type'          => 'text',
        'settings'      => 'faq_question_three',
        'label'         => esc_attr__( 'FAQ Question Three', 'turbulence' ),
        'description'   => esc_attr__( 'Enter Question Three', 'turbulence' ),
        'section'       => 'faq_three_section',
        'default'       => '',
        'priority'      => 10,
    ) );
    // faq answer three control
    Kirki::add_field( 'faq_answer_three', array(
        'type'        => 'textarea',
        'settings'    => 'faq_answer_three',
        'label'       => esc_attr__( 'FAQ Answer Three', 'turbulence' ),
        'description' => esc_attr__( 'Enter your Answer for Question Three', 'turbulence' ),
        'section'     => 'faq_three_section',
        'default'     => '',
        'priority'    => 10,
    ) );

/*========= Question / Answer Four =========*/
    // faq four section
    Kirki::add_section( 'faq_four_section', array(
        'priority'          => 10,
        'title'          => esc_attr__( 'FAQ Four Section', 'turbulence' ),
        'priority'       => 1,
        'capability'     => 'edit_theme_options',
        'panel'             => 'faq',
    ) );
    // faq question four control
    Kirki::add_field( 'faq_question_four', array(
        'type'          => 'text',
        'settings'      => 'faq_question_four',
        'label'         => esc_attr__( 'FAQ Question Four', 'turbulence' ),
        'description'   => esc_attr__( 'Enter Question Four', 'turbulence' ),
        'section'       => 'faq_four_section',
        'default'       => '',
        'priority'      => 10,
    ) );
    // faq answer four control
    Kirki::add_field( 'faq_answer_four', array(
        'type'        => 'textarea',
        'settings'    => 'faq_answer_four',
        'label'       => esc_attr__( 'FAQ Answer Four', 'turbulence' ),
        'description' => esc_attr__( 'Enter your Answer for Question Four', 'turbulence' ),
        'section'     => 'faq_four_section',
        'default'     => '',
        'priority'    => 10,
    ) );

/*========= Question / Answer Five =========*/
    // faq five section
    Kirki::add_section( 'faq_five_section', array(
        'priority'          => 10,
        'title'          => esc_attr__( 'FAQ Five Section', 'turbulence' ),
        'priority'       => 1,
        'capability'     => 'edit_theme_options',
        'panel'             => 'faq',
    ) );
    // faq question five control
    Kirki::add_field( 'faq_question_five', array(
        'type'          => 'text',
        'settings'      => 'faq_question_five',
        'label'         => esc_attr__( 'FAQ Question Five', 'turbulence' ),
        'description'   => esc_attr__( 'Enter Question Five', 'turbulence' ),
        'section'       => 'faq_five_section',
        'default'       => '',
        'priority'      => 10,
    ) );
    // faq answer five control
    Kirki::add_field( 'faq_answer_five', array(
        'type'        => 'textarea',
        'settings'    => 'faq_answer_five',
        'label'       => esc_attr__( 'FAQ Answer Five', 'turbulence' ),
        'description' => esc_attr__( 'Enter your Answer for Question Five', 'turbulence' ),
        'section'     => 'faq_five_section',
        'default'     => '',
        'priority'    => 10,
    ) );

      

/*========= Question / Answer Six =========*/
    // faq six section
    Kirki::add_section( 'faq_six_section', array(
        'priority'          => 10,
        'title'          => esc_attr__( 'FAQ Six Section', 'turbulence' ),
        'priority'       => 1,
        'capability'     => 'edit_theme_options',
        'panel'             => 'faq',
    ) );
    // faq question six control
    Kirki::add_field( 'faq_question_six', array(
        'type'          => 'text',
        'settings'      => 'faq_question_six',
        'label'         => esc_attr__( 'FAQ Question Six', 'turbulence' ),
        'description'   => esc_attr__( 'Enter Question Six', 'turbulence' ),
        'section'       => 'faq_six_section',
        'default'       => '',
        'priority'      => 10,
    ) );
    // faq answer six control
    Kirki::add_field( 'faq_answer_six', array(
        'type'        => 'textarea',
        'settings'    => 'faq_answer_six',
        'label'       => esc_attr__( 'FAQ Answer Six', 'turbulence' ),
        'description' => esc_attr__( 'Enter your Answer for Question Six', 'turbulence' ),
        'section'     => 'faq_six_section',
        'default'     => '',
        'priority'    => 10,
    ) );

/*========= Question / Answer Seven =========*/
    // faq seven section
    Kirki::add_section( 'faq_seven_section', array(
        'priority'          => 10,
        'title'          => esc_attr__( 'FAQ Seven Section', 'turbulence' ),
        'priority'       => 1,
        'capability'     => 'edit_theme_options',
        'panel'             => 'faq',
    ) );
    // faq question seven control
    Kirki::add_field( 'faq_question_seven', array(
        'type'          => 'text',
        'settings'      => 'faq_question_seven',
        'label'         => esc_attr__( 'FAQ Question Seven', 'turbulence' ),
        'description'   => esc_attr__( 'Enter Question Seven', 'turbulence' ),
        'section'       => 'faq_seven_section',
        'default'       => '',
        'priority'      => 10,
    ) );
    // faq answer seven control
    Kirki::add_field( 'faq_answer_seven', array(
        'type'        => 'textarea',
        'settings'    => 'faq_answer_seven',
        'label'       => esc_attr__( 'FAQ Answer Seven', 'turbulence' ),
        'description' => esc_attr__( 'Enter your Answer for Question Seven', 'turbulence' ),
        'section'     => 'faq_seven_section',
        'default'     => '',
        'priority'    => 10,
    ) );

/*========= Question / Answer Eight =========*/
    // faq eight section
    Kirki::add_section( 'faq_eight_section', array(
        'priority'          => 10,
        'title'          => esc_attr__( 'FAQ Eight Section', 'turbulence' ),
        'priority'       => 1,
        'capability'     => 'edit_theme_options',
        'panel'             => 'faq',
    ) );
    // faq question eight control
    Kirki::add_field( 'faq_question_eight', array(
        'type'          => 'text',
        'settings'      => 'faq_question_eight',
        'label'         => esc_attr__( 'FAQ Question Eight', 'turbulence' ),
        'description'   => esc_attr__( 'Enter Question Eight', 'turbulence' ),
        'section'       => 'faq_eight_section',
        'default'       => '',
        'priority'      => 10,
    ) );
    // faq answer eight control
    Kirki::add_field( 'faq_answer_eight', array(
        'type'        => 'textarea',
        'settings'    => 'faq_answer_eight',
        'label'       => esc_attr__( 'FAQ Answer Eight', 'turbulence' ),
        'description' => esc_attr__( 'Enter your Answer for Question Eight', 'turbulence' ),
        'section'     => 'faq_eight_section',
        'default'     => '',
        'priority'    => 10,
    ) );

/*========= Question / Answer Nine =========*/
    // faq nine section
     Kirki::add_section( 'faq_nine_section', array(
        'priority'          => 10,
        'title'          => esc_attr__( 'FAQ Nine Section', 'turbulence' ),
        'priority'       => 1,
        'capability'     => 'edit_theme_options',
        'panel'             => 'faq',
    ) );
    // faq question nine control
    Kirki::add_field( 'faq_question_nine', array(
        'type'          => 'text',
        'settings'      => 'faq_question_nine',
        'label'         => esc_attr__( 'FAQ Question Nine', 'turbulence' ),
        'description'   => esc_attr__( 'Enter Question Nine', 'turbulence' ),
        'section'       => 'faq_nine_section',
        'default'       => '',
        'priority'      => 10,
    ) );
    // faq answer nine control
    Kirki::add_field( 'faq_answer_nine', array(
        'type'        => 'textarea',
        'settings'    => 'faq_answer_nine',
        'label'       => esc_attr__( 'FAQ Answer Nine', 'turbulence' ),
        'description' => esc_attr__( 'Enter your Answer for Question Nine', 'turbulence' ),
        'section'     => 'faq_nine_section',
        'default'     => '',
        'priority'    => 10,
    ) );
       

/*========= Question / Answer Ten =========*/
     // faq ten section
     Kirki::add_section( 'faq_ten_section', array(
        'priority'          => 10,
        'title'          => esc_attr__( 'FAQ Ten Section', 'turbulence' ),
        'priority'       => 1,
        'capability'     => 'edit_theme_options',
        'panel'             => 'faq',
    ) );
    // faq question nine control
    Kirki::add_field( 'faq_question_ten', array(
        'type'          => 'text',
        'settings'      => 'faq_question_ten',
        'label'         => esc_attr__( 'FAQ Question Ten', 'turbulence' ),
        'description'   => esc_attr__( 'Enter Question Ten', 'turbulence' ),
        'section'       => 'faq_ten_section',
        'default'       => '',
        'priority'      => 10,
    ) );
    // faq answer nine control
    Kirki::add_field( 'faq_answer_ten', array(
        'type'        => 'textarea',
        'settings'    => 'faq_answer_ten',
        'label'       => esc_attr__( 'FAQ Answer Ten', 'turbulence' ),
        'description' => esc_attr__( 'Enter your Answer for Question Ten', 'turbulence' ),
        'section'     => 'faq_ten_section',
        'default'     => '',
        'priority'    => 10,
    ) );