<?php 

// Bootstrap Theme Stylesheets
	$bootstrap_theme_stylesheets = array (
		"bootstrap.min.css"     => esc_attr__( 'Bootstrap', 'turbulence' ),
		"cerulean.css"          => esc_attr__( 'Cerulean', 'turbulence' ),
        "cosmo.css"             => esc_attr__( 'Cosmo', 'turbulence' ),
        "cyborg.css"		    => esc_attr__( 'Cyborg', 'turbulence' ),
        "darkly.css"            => esc_attr__( 'Darkly', 'turbulence' ),
		"flatly.css"            => esc_attr__( 'Flatly', 'turbulence' ),
		"journal.css"           => esc_attr__( 'Journal', 'turbulence' ),
		"lumen.css"             => esc_attr__( 'Lumen', 'turbulence' ),
		"paper.css"             => esc_attr__( 'Paper', 'turbulence' ),
		"readable.css"          => esc_attr__( 'Readable', 'turbulence' ),
		"sandstone.css"         => esc_attr__( 'Sandstone', 'turbulence' ),
		"simplex.css"           => esc_attr__( 'Simplex', 'turbulence' ),
		"slate.css"             => esc_attr__( 'Slate', 'turbulence' ),
		"spacelab.css"          => esc_attr__( 'Spacelab', 'turbulence' ),
		"superhero.css"         => esc_attr__( 'Superhero', 'turbulence' ),
		"united.css"            => esc_attr__( 'United', 'turbulence' ),
		"yeti.css"              => esc_attr__( 'Yeti', 'turbulence' ),
	);

/*========= General Settings Section =========*/
    // general settings section    
    Kirki::add_section( 'general_settings', array(
        'priority'       => 10,
        'title'          => esc_attr__( 'General Settings', 'turbulence' ),
        'priority'       => 1,
        'capability'     => 'edit_theme_options',
    ) );
       
    // bootstrap theme stylesheets
    Kirki::add_field( 'bootstrap_themes', array(
        'type'        => 'select',
        'settings'    => 'bootstrap_themes',
        'label'       => esc_attr__( 'Bootstrap Themes Stylesheets', 'turbulence' ),
        'description' => esc_attr__( 'Select your themes alternative color scheme.', 'turbulence' ),
        'section'     => 'general_settings',
        'default'       => 'bootstrap.min.css',
        'priority'    => 10,
        'choices'     => $bootstrap_theme_stylesheets,
    ) );
     
/*=======================================

Favicon Upload Control is located in the functions file. 
At the time of this theme creation, Kirki does not have
Image Upload controls yet. So I used the WordPress Theme
Customizer API to create the Favicon control.

=========================================*/

    // custom css control
    Kirki::add_field( 'custom_css', array(
        'type'        => 'code',
        'settings'    => 'custom_css',
        'label'       => esc_attr__( 'Add custom CSS here', 'turbulence' ),
        'description' => esc_attr__( 'Insert any custom CSS.', 'turbulence' ),
        'section'     => 'general_settings',
        'default'     => '',
        'priority'    => 20,
        'choices'     => array(
            'language' => 'css',
            'theme'    => 'monokai',
            'height'   => 250,
        ),
    ) );  