<?php 

/*========= Custom Header Settings Section =========*/
    // header section    
    Kirki::add_section( 'turbulence_header_section', array(
        'priority'       => 10,
        'title'          => esc_attr__( 'Header Section', 'turbulence' ),
        'priority'       => 1,
        'capability'     => 'edit_theme_options',
    ) );
    // navbar fixed enable control
    Kirki::add_field( 'turbulence_navbar_fixed', array(
        'type'        => 'switch',
        'settings'    => 'turbulence_navbar_fixed',
        'label'       => esc_attr__( 'Navbar Fixed', 'turbulence' ),
        'description' => esc_attr__( 'Select to enable/disable a fixed navbar.', 'turbulence' ),
        'section'     => 'turbulence_header_section',
        'default'     => true,
        'priority'    => 10,
        'required'    => array(
            array(
                'operator' => '==',
                'value'    => true,
            ),
        ),
    ) );
    // navbar inverse enable control
    Kirki::add_field( 'turbulence_navbar_inverse', array(
        'type'        => 'switch',
        'settings'    => 'turbulence_navbar_inverse',
        'label'       => esc_attr__( 'Inverse Navbar', 'turbulence' ),
        'description' => esc_attr__( 'Select to enable/disable an inverse navbar color.', 'turbulence' ),
        'section'     => 'turbulence_header_section',
        'default'     => true,
        'priority'    => 10,
        'required'    => array(
            array(
                'operator' => '==',
                'value'    => true,
            ),
        ),
    ) );
/*=======================================

Logo Control is located in the functions file. 
At the time of this theme creation, Kirki does not have
Image Upload controls yet. So I used the WordPress Theme
Customizer API to create the Favicon control.

=========================================*/  

       
        