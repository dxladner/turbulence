<?php 

/*========= Portfolio Settings Section =========*/
        // portfolio settings section
        Kirki::add_section( 'turbulence_portfolio_section', array(
            'priority'       => 10,
            'title'          => esc_attr__( 'Portfolio Settings', 'turbulence' ),
            'priority'       => 1,
            'capability'     => 'edit_theme_options',
        ) );
        // portfolio columns control
        Kirki::add_field( 'turbulence_portfolio_columns', array(
                'type'        => 'radio-buttonset',
                'settings'    => 'turbulence_portfolio_columns',
                'label'       => esc_attr__( 'Portfolio Columns', 'turbulence' ),
                'description' => esc_attr__( 'Select the number of columns you would like to use for the portfolio.', 'turbulence' ),
                'section'     => 'turbulence_portfolio_section',
                'default'     => 'default',
                'priority'    => 10,
                'choices'     => array(
                    '6'     => 'Two Column',
                    '4'     => 'Three Column',
                    '3'     => 'Four Column',
                ),
        ) );
        // portfolio display filter control
        Kirki::add_field( 'turbulence_display_filter', array(
            'type'        => 'switch',
            'settings'    => 'turbulence_display_filter',
            'label'       => esc_attr__( 'Display Filter', 'turbulence' ),
            'description' => esc_attr__( 'Select to enable/disable the portfolio filter.', 'turbulence' ),
            'section'     => 'turbulence_portfolio_section',
            'default'     => true,
                'priority'    => 10,
                'required'    => array(
                    array(
                        'operator' => '==',
                        'value'    => true,
                ),
            ),
        ) ); 
        // portfolio display filter button size control
        Kirki::add_field( 'turbulence_display_filter_button_size', array(
                'type'        => 'radio-buttonset',
                'settings'    => 'turbulence_display_filter_button_size',
                'label'       => esc_attr__( 'Filter Button Size', 'turbulence' ),
                'description' => esc_attr__( 'Select the Bootstrap button size you want for the Filter.', 'turbulence' ),
                'section'     => 'turbulence_portfolio_section',
                'default'     => 'default',
                'priority'    => 10,
                'choices'     => array(
                        'xs'            => esc_attr__( 'Extra Small', 'turbulence' ),
                        'sm'            => esc_attr__( 'Small', 'turbulence' ),
                        'default'       => esc_attr__( 'Medium', 'turbulence' ),
                        'lg'            => esc_attr__( 'Large', 'turbulence' ),
                ),
        ) );
        // portfolio display filter button color control
        Kirki::add_field( 'turbulence_display_filter_button_color', array(
                'type'        => 'select',
                'settings'    => 'turbulence_display_filter_button_color',
                'label'       => esc_attr__( 'Filter Button Color', 'turbulence' ),
                'description' => esc_attr__( 'Select the Bootstrap button color you want for the filter.', 'turbulence' ),
                'section'     => 'turbulence_portfolio_section',
                'default'     => 'primary',
                'priority'    => 10,
                'choices'     => array(
                        'default'       => esc_attr__( 'Default', 'turbulence' ),
                        'primary'       => esc_attr__( 'Primary', 'turbulence' ),
                        'info'          => esc_attr__( 'Info', 'turbulence' ),
                        'success'       => esc_attr__( 'Success', 'turbulence' ),
                        'warning'       => esc_attr__( 'Warning', 'turbulence' ),
                        'danger'        => esc_attr__( 'Danger', 'turbulence' ),
                        'link'          => esc_attr__( 'Link', 'turbulence' ),
                ),
        ) );