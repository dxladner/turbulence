<?php
    $social_options = array(
          ''             => esc_attr__( 'Choose an Icon', 'turbulence' ),
         'twitter'       => esc_attr__( 'Twitter', 'turbulence' ),
         'facebook'      => esc_attr__( 'Facebook', 'turbulence' ),
         'google-plus'   => esc_attr__( 'Google Plus', 'turbulence' ),
         'instagram'     => esc_attr__( 'Instagram', 'turbulence' ),
         'linkedin'      => esc_attr__( 'LinkedIn', 'turbulence' ),
         'tumblr'        => esc_attr__( 'Tumblr', 'turbulence' ),
         'pinterest'     => esc_attr__( 'Pinterest', 'turbulence' ),
         'github'        => esc_attr__( 'Github', 'turbulence' ),
         'bitbucket'     => esc_attr__( 'BitBucket', 'turbulence' ),
         'dribbble'      => esc_attr__( 'Dribbble', 'turbulence' ),
         'flickr'        => esc_attr__( 'Flickr', 'turbulence' ),
         'skype'         => esc_attr__( 'Skype', 'turbulence' ),
         'youtube'       => esc_attr__( 'Youtube', 'turbulence' ),
         'vimeo-square'  => esc_attr__( 'Vimeo', 'turbulence' ),
         'reddit'        => esc_attr__( 'Reddit', 'turbulence' ),
         'stumbleupon'   => esc_attr__( 'Stumbleupon', 'turbulence' ),
         'rss'           => esc_attr__( 'RSS', 'turbulence' ),
    );

/*========= Social Media Icons Section =========*/
        // Create custom panel for Social Media Icons Section
        Kirki::add_panel( 'social_media', array(
            'priority'    => 10,
            'title'       => __( 'Social Media Icons', 'turbulence' ),
            'description' => __( 'Social Media Icons Settings', 'turbulence' ),
        ) );
/*========= Social Media Icons One Section =========*/
        // social media icon one section
        Kirki::add_section( 'social_media_icon_one_section', array(
            'priority'       => 10,
            'title'          => esc_attr__( 'Social Media Icon One', 'turbulence' ),
            'priority'       => 1,
            'capability'     => 'edit_theme_options',
            'panel'         => 'social_media',
        ) );
        // social media icon one Control
        Kirki::add_field( 'social_media_icon_one', array(
            'type'        => 'select',
            'settings'    => 'social_media_icon_one',
            'label'       => esc_attr__( 'Choose a Social Media Icon', 'turbulence' ),
            'section'     => 'social_media_icon_one_section',
            'default'     => '',
            'priority'    => 10,
            'choices'     => $social_options,
        ) );
        // social media one user id Control
        Kirki::add_field( 'social_media_one_user_id', array(
            'type'          => 'text',
            'settings'      => 'social_media_one_user_id',
            'label'         => esc_attr__( 'Social Media One User ID', 'turbulence' ),
            'description'   => esc_attr__( 'Enter your Social Media User ID', 'turbulence' ),
            'section'       => 'social_media_icon_one_section',
            'default'       => '',
            'priority'      => 10,
        ) );


/*========= Social Media Icons Two Section =========*/
        // social media icon two section
        Kirki::add_section( 'social_media_icon_two_section', array(
            'priority'       => 10,
            'title'          => esc_attr__( 'Social Media Icon Two', 'turbulence' ),
            'priority'       => 1,
            'capability'     => 'edit_theme_options',
             'panel'         => 'social_media',
        ) );
        // social media icon two Control
        Kirki::add_field( 'social_media_icon_two', array(
                'type'        => 'select',
                'settings'    => 'social_media_icon_two',
                'label'       => esc_attr__( 'Choose a Social Media Icon', 'turbulence' ),
                'section'     => 'social_media_icon_two_section',
                'default'     => 'green',
                'priority'    => 10,
                'choices'     => $social_options,
        ) );
        // social media two user id Control
        Kirki::add_field( 'social_media_two_user_id', array(
            'type'          => 'text',
            'settings'      => 'social_media_two_user_id',
            'label'         => esc_attr__( 'Social Media Two User ID', 'turbulence' ),
            'description'   => esc_attr__( 'Enter your Social Media User ID', 'turbulence' ),
            'section'       => 'social_media_icon_two_section',
            'default'       => '',
            'priority'      => 10,
        ) );

/*========= Social Media Icons Three Section =========*/
        // social media icon three section
        Kirki::add_section( 'social_media_icon_three_section', array(
            'priority'       => 10,
            'title'          => esc_attr__( 'Social Media Icon Three', 'turbulence' ),
            'priority'       => 1,
            'capability'     => 'edit_theme_options',
             'panel'         => 'social_media',
        ) );
        // social media icon three Control
        Kirki::add_field( 'social_media_icon_three', array(
                'type'        => 'select',
                'settings'    => 'social_media_icon_three',
                'label'       => esc_attr__( 'Choose a Social Media Icon', 'turbulence' ),
                'section'     => 'social_media_icon_three_section',
                'default'     => 'green',
                'priority'    => 10,
                'choices'     => $social_options,
        ) );
        // social media three user id Control
        Kirki::add_field( 'social_media_three_user_id', array(
            'type'          => 'text',
            'settings'      => 'social_media_three_user_id',
            'label'         => esc_attr__( 'Social Media Three User ID', 'turbulence' ),
            'description'   => esc_attr__( 'Enter your Social Media User ID', 'turbulence' ),
            'section'       => 'social_media_icon_three_section',
            'default'       => '',
            'priority'      => 10,
        ) );

/*========= Social Media Icons Four Section =========*/
        // social media icon four section
        Kirki::add_section( 'social_media_icon_four_section', array(
            'priority'       => 10,
            'title'          => esc_attr__( 'Social Media Icon Four', 'turbulence' ),
            'priority'       => 1,
            'capability'     => 'edit_theme_options',
             'panel'         => 'social_media',
        ) );
        // social media icon four Control
        Kirki::add_field( 'social_media_icon_four', array(
                'type'        => 'select',
                'settings'    => 'social_media_icon_four',
                'label'       => esc_attr__( 'Choose a Social Media Icon', 'turbulence' ),
                'section'     => 'social_media_icon_four_section',
                'default'     => 'green',
                'priority'    => 10,
                'choices'     => $social_options,
        ) );
        // social media four user id Control
        Kirki::add_field( 'social_media_four_user_id', array(
            'type'          => 'text',
            'settings'      => 'social_media_four_user_id',
            'label'         => esc_attr__( 'Social Media Four User ID', 'turbulence' ),
            'description'   => esc_attr__( 'Enter your Social Media User ID', 'turbulence' ),
            'section'       => 'social_media_icon_four_section',
            'default'       => '',
            'priority'      => 10,
        ) );
       
/*========= Social Media Icons Five Section =========*/
        // social media icon five section
        Kirki::add_section( 'social_media_icon_five_section', array(
            'priority'       => 10,
            'title'          => esc_attr__( 'Social Media Icon Five', 'turbulence' ),
            'priority'       => 1,
            'capability'     => 'edit_theme_options',
             'panel'         => 'social_media',
        ) );
        // social media icon five Control
        Kirki::add_field( 'social_media_icon_five', array(
                'type'        => 'select',
                'settings'    => 'social_media_icon_five',
                'label'       => esc_attr__( 'Choose a Social Media Icon', 'turbulence' ),
                'section'     => 'social_media_icon_five_section',
                'default'     => 'green',
                'priority'    => 10,
                'choices'     => $social_options,
        ) );
        // social media five user id Control
        Kirki::add_field( 'social_media_five_user_id', array(
            'type'          => 'text',
            'settings'      => 'social_media_five_user_id',
            'label'         => esc_attr__( 'Social Media Five User ID', 'turbulence' ),
            'description'   => esc_attr__( 'Enter your Social Media User ID', 'turbulence' ),
            'section'       => 'social_media_icon_five_section',
            'default'       => '',
            'priority'      => 10,
        ) );
       
/*========= Social Media Icons Six Section =========*/
        // social media icon five section
        Kirki::add_section( 'social_media_icon_six_section', array(
            'priority'       => 10,
            'title'          => esc_attr__( 'Social Media Icon Six', 'turbulence' ),
            'priority'       => 1,
            'capability'     => 'edit_theme_options',
             'panel'         => 'social_media',
        ) );
        // social media icon five Control
        Kirki::add_field( 'social_media_icon_six', array(
                'type'        => 'select',
                'settings'    => 'social_media_icon_six',
                'label'       => esc_attr__( 'Choose a Social Media Icon', 'turbulence' ),
                'section'     => 'social_media_icon_six_section',
                'default'     => 'green',
                'priority'    => 10,
                'choices'     => $social_options,
        ) );
        // social media five user id Control
        Kirki::add_field( 'social_media_six_user_id', array(
            'type'          => 'text',
            'settings'      => 'social_media_six_user_id',
            'label'         => esc_attr__( 'Social Media Six User ID', 'turbulence' ),
            'description'   => esc_attr__( 'Enter your Social Media User ID', 'turbulence' ),
            'section'       => 'social_media_icon_six_section',
            'default'       => '',
            'priority'      => 10,
        ) );
         
/*========= Social Media Icons Seven Section =========*/
        // social media icon seven section
        Kirki::add_section( 'social_media_icon_seven_section', array(
            'priority'       => 10,
            'title'          => esc_attr__( 'Social Media Icon Seven', 'turbulence' ),
            'priority'       => 1,
            'capability'     => 'edit_theme_options',
             'panel'         => 'social_media',
        ) );
        // social media icon seven Control
        Kirki::add_field( 'social_media_icon_seven', array(
                'type'        => 'select',
                'settings'    => 'social_media_icon_seven',
                'label'       => esc_attr__( 'Choose a Social Media Icon', 'turbulence' ),
                'section'     => 'social_media_icon_seven_section',
                'default'     => 'green',
                'priority'    => 10,
                'choices'     => $social_options,
        ) );
        // social media seven user id Control
        Kirki::add_field( 'social_media_seven_user_id', array(
            'type'          => 'text',
            'settings'      => 'social_media_seven_user_id',
            'label'         => esc_attr__( 'Social Media Seven User ID', 'turbulence' ),
            'description'   => esc_attr__( 'Enter your Social Media User ID', 'turbulence' ),
            'section'       => 'social_media_icon_seven_section',
            'default'       => '',
            'priority'      => 10,
        ) );
        
/*========= Social Media Icons Eight Section =========*/
        // social media icon eight section
        Kirki::add_section( 'social_media_icon_eight_section', array(
            'priority'       => 10,
            'title'          => esc_attr__( 'Social Media Icon Eight', 'turbulence' ),
            'priority'       => 1,
            'capability'     => 'edit_theme_options',
             'panel'         => 'social_media',
        ) );
        // social media icon eight Control
        Kirki::add_field( 'social_media_icon_eight', array(
                'type'        => 'select',
                'settings'    => 'social_media_icon_eight',
                'label'       => esc_attr__( 'Choose a Social Media Icon', 'turbulence' ),
                'section'     => 'social_media_icon_eight_section',
                'default'     => 'green',
                'priority'    => 10,
                'choices'     => $social_options,
        ) );
        // social media eight user id Control
        Kirki::add_field( 'social_media_eight_user_id', array(
            'type'          => 'text',
            'settings'      => 'social_media_eight_user_id',
            'label'         => esc_attr__( 'Social Media Eight User ID', 'turbulence' ),
            'description'   => esc_attr__( 'Enter your Social Media User ID', 'turbulence' ),
            'section'       => 'social_media_icon_eight_section',
            'default'       => '',
            'priority'      => 10,
        ) );
       
/*========= Social Media Icons Nine Section =========*/
        // social media icon nine section
        Kirki::add_section( 'social_media_icon_nine_section', array(
            'priority'       => 10,
            'title'          => esc_attr__( 'Social Media Icon Nine', 'turbulence' ),
            'priority'       => 1,
            'capability'     => 'edit_theme_options',
             'panel'         => 'social_media',
        ) );
        // social media icon nine Control
        Kirki::add_field( 'social_media_icon_nine', array(
                'type'        => 'select',
                'settings'    => 'social_media_icon_nine',
                'label'       => esc_attr__( 'Choose a Social Media Icon', 'turbulence' ),
                'section'     => 'social_media_icon_nine_section',
                'default'     => 'green',
                'priority'    => 10,
                'choices'     => $social_options,
        ) );
        // social media nine user id Control
        Kirki::add_field( 'social_media_nine_user_id', array(
            'type'          => 'text',
            'settings'      => 'social_media_nine_user_id',
            'label'         => esc_attr__( 'Social Media Nine User ID', 'turbulence' ),
            'description'   => esc_attr__( 'Enter your Social Media User ID', 'turbulence' ),
            'section'       => 'social_media_icon_nine_section',
            'default'       => '',
            'priority'      => 10,
        ) );
        
/*========= Social Media Icons Ten Section =========*/
        // social media icon ten section
        Kirki::add_section( 'social_media_icon_ten_section', array(
            'priority'       => 10,
            'title'          => esc_attr__( 'Social Media Icon Ten', 'turbulence' ),
            'priority'       => 1,
            'capability'     => 'edit_theme_options',
             'panel'         => 'social_media',
        ) );
        // social media icon ten Control
        Kirki::add_field( 'social_media_icon_ten', array(
                'type'        => 'select',
                'settings'    => 'social_media_icon_ten',
                'label'       => esc_attr__( 'Choose a Social Media Icon', 'turbulence' ),
                'section'     => 'social_media_icon_ten_section',
                'default'     => 'green',
                'priority'    => 10,
                'choices'     => $social_options,
        ) );
        // social media ten user id Control
        Kirki::add_field( 'social_media_ten_user_id', array(
            'type'          => 'text',
            'settings'      => 'social_media_ten_user_id',
            'label'         => esc_attr__( 'Social Media Ten User ID', 'turbulence' ),
            'description'   => esc_attr__( 'Enter your Social Media User ID', 'turbulence' ),
            'section'       => 'social_media_icon_ten_section',
            'default'       => '',
            'priority'      => 10,
        ) );
        
        
        
