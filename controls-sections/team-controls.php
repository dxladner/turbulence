<?php

 /*========= Custom Team Settings Panel =========*/
        Kirki::add_panel( 'team', array(
            'priority'    => 10,
            'title'       => __( 'Team', 'turbulence' ),
            'description' => __( 'Team Column Settings Section', 'turbulence' ),
        ) );
/*========= Custom Team Section =========*/
        // team column section
        Kirki::add_section( 'team_page_column_settings_section', array(
            'priority'       => 10,
            'title'          => esc_attr__( 'Team Page Columns', 'turbulence' ),
            'priority'       => 1,
            'capability'     => 'edit_theme_options',
            'panel'         => 'team',
        ) );            
        // team page columns control
        Kirki::add_field( 'team_page_columns', array(
                'type'        => 'radio-buttonset',
                'settings'    => 'team_page_columns',
                'label'       => esc_attr__( 'Team Columns', 'turbulence' ),
                'description' => esc_attr__( 'Select the number of columns you would like to use for the team member page.', 'turbulence' ),
                'section'     => 'team_page_column_settings_section',
                'default'     => '4',
                'priority'    => 10,
                'choices'     => array(
                    '6'     => 'Two Column',
                    '4'     => 'Three Column',
                    '3'     => 'Four Column',
                ),
        ) );
       
/*========= Custom Team Page Body Section =========*/
        // team column section
        Kirki::add_section( 'team_page_body_section', array(
            'priority'       => 10,
            'title'          => esc_attr__( 'Team Page Body Settings', 'turbulence' ),
            'priority'       => 1,
            'capability'     => 'edit_theme_options',
            'panel'         => 'team',
        ) );  
        // team page body background color control
        Kirki::add_field( 'team_page_body_background_color', array(
            'type'        => 'color',
            'settings'    => 'team_page_body_background_color',
            'label'       => esc_attr__( 'Team Page Background Color', 'turbulence' ),
            'description' => esc_attr__( 'Team Page Body Background', 'turbulence' ),
            'section'     => 'team_page_body_section',
            'default'     => '#2c3e50',
            'priority'    => 10,
        ) );
      
       
/*========= Custom Team Page Settings Section =========*/
        // team column section
        Kirki::add_section( 'team_page_settings_section', array(
            'priority'       => 10,
            'title'          => esc_attr__( 'Team Page Settings', 'turbulence' ),
            'priority'       => 1,
            'capability'     => 'edit_theme_options',
            'panel'         => 'team',
        ) ); 
        // team page images background color control
        Kirki::add_field( 'team_page_images_background_color', array(
            'type'        => 'color',
            'settings'    => 'team_page_images_background_color',
            'label'       => esc_attr__( 'Team Page Images Background Color', 'turbulence' ),
            'description' => esc_attr__( 'Pick a color for the Team Page Image background', 'turbulence' ),
            'section'     => 'team_page_settings_section',
            'default'     => '#2c3e50',
            'priority'    => 10,
        ) );
        // team page images background color control
        Kirki::add_field( 'team_page_header_text_color', array(
            'type'        => 'color',
            'settings'    => 'team_page_header_text_color',
            'label'       => esc_attr__( 'Team Page Header Text Color', 'turbulence' ),
            'description' => esc_attr__( 'Pick a color for the Team Page Text background', 'turbulence' ),
            'section'     => 'team_page_settings_section',
            'default'     => '#ffffff',
            'priority'    => 10,
        ) );
        // team page member name color control
        Kirki::add_field( 'team_page_member_name_text_color', array(
            'type'        => 'color',
            'settings'    => 'team_page_member_name_text_color',
            'label'       => esc_attr__( 'Team Page Member Name Text Color', 'turbulence' ),
            'description' => esc_attr__( 'Pick a color for the Team Page Text background', 'turbulence' ),
            'section'     => 'team_page_settings_section',
            'default'     => '#ffffff',
            'priority'    => 10,
        ) );
        // team page member name color control
         Kirki::add_field( 'team_page_member_job_title_text_color', array(
            'type'        => 'color',
            'settings'    => 'team_page_member_job_title_text_color',
            'label'       => esc_attr__( 'Team Page Member Job Title Text Color', 'turbulence' ),
            'description' => esc_attr__( 'Pick a color for the Team Page Text background', 'turbulence' ),
            'section'     => 'team_page_settings_section',
            'default'     => '#ffffff',
            'priority'    => 10,
        ) );
        
/*=============== Single Page Team Details ==================*/

        // team single page member header control
        Kirki::add_field( 'team_single_page_member_header', array(
            'type'          => 'text',
            'settings'      => 'team_single_page_member_header',
            'label'         => esc_attr__( 'Team Single Page Member Header', 'turbulence' ),
            'description'   => esc_attr__( 'This is the heading for the Team Single Page Member Header.', 'turbulence' ),
            'section'       => 'team_page_settings_section',
            'default'       => '',
            'priority'      => 10,
        ) ); 
        // team single page member paragraph control
        Kirki::add_field( 'team_single_page_member_paragraph', array(
                'type'        => 'textarea',
                'settings'    => 'team_single_page_member_paragraph',
                'label'       => esc_attr__( 'Team Single Page Member Paragraph', 'turbulence' ),
                'description' => esc_attr__( 'This is the content of the Team Single Page Member Paragraph.', 'turbulence' ),
                'section'     => 'team_page_settings_section',
                'default'     => '',
                'priority'    => 10,
        ) ); 
        // team single page member panel header color control
        Kirki::add_field( 'team_single_page_member_panel_header_color', array(
                'type'        => 'select',
                'settings'    => 'team_single_page_member_panel_header_color',
                'label'       => esc_attr__( 'Team Single Page Member Panel Header Color', 'turbulence' ),
                'description' => esc_attr__( 'Select the Bootstrap button color you want for the Team Single Page Member Panel Header.', 'turbulence' ),
                'section'     => 'team_page_settings_section',
                'default'     => 'primary',
                'priority'    => 10,
                'choices'     => array(
                        'default'       => esc_attr__( 'Default', 'turbulence' ),
                        'primary'       => esc_attr__( 'Primary', 'turbulence' ),
                        'info'          => esc_attr__( 'Info', 'turbulence' ),
                        'success'       => esc_attr__( 'Success', 'turbulence' ),
                        'warning'       => esc_attr__( 'Warning', 'turbulence' ),
                        'danger'        => esc_attr__( 'Danger', 'turbulence' ),
                        'link'          => esc_attr__( 'Link', 'turbulence' ),
                ),
        ) );