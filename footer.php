<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package turbulence
 */
?>
		
		<footer>
				<div class="footer">
					<div class="container">
						<div class="row">
							<div class="col-sm-4 col-md-4 footer-widget">
							<?php
                				if(is_active_sidebar('footer-one-widget-area')){
                				dynamic_sidebar('footer-one-widget-area');
                				}
            				?>
							</div>
							<div class="col-sm-4 col-md-4 footer-widget">
						    <?php
                				if(is_active_sidebar('footer-two-widget-area')){
                				dynamic_sidebar('footer-two-widget-area');
                				}
            				?>
							</div>
							<div class="col-sm-4 col-md-4 footer-widget clearfix">
    							<?php
                				if(is_active_sidebar('footer-three-widget-area')){
                				dynamic_sidebar('footer-three-widget-area');
                				}
            				?>
							</div>
						</div>
					</div>
				</div>
				<div class="copyright">
					<div class="row">
    					<div class="col-md-1">
    					</div>
						<div class="col-md-3">
							<?php if (get_theme_mod('turbulence_custom_copyright') != '') { ?>
							<div class="copyright">
								<?php echo get_theme_mod('turbulence_custom_copyright'); ?>
							</div>
							<?php } ?>
						</div>
						<div class="col-md-1">
						<?php if (get_theme_mod('footer_icon') != '') { ?>
							<div class="ft-icon">
								<?php $footer_fa_icon = get_theme_mod('footer_icon'); ?>
								<i class="fa fa-<?php echo $footer_fa_icon; ?> fa-2x"></i>
							</div>
							<?php } ?>
    					</div>
						<div class="col-md-3">
							<?php if (get_theme_mod('turbulence_custom_text') != '') { ?>
							<div class="poweredby">
								<?php echo get_theme_mod('turbulence_custom_text'); ?>
							</div>
							<?php } ?>	
						</div>
						<div class="col-md-4">
								<div class="social-icons">
										<?php
										$social_options = array(); 
										if (get_theme_mod('social_media_icon_one') != '')
										{
											$social_media_icon_one = get_theme_mod('social_media_icon_one');
											$social_media_one_user_id = get_theme_mod('social_media_one_user_id');
											array_push($social_options, $social_media_icon_one, $social_media_one_user_id);
										}

										if (get_theme_mod('social_media_icon_two') != '')
										{
											$social_media_icon_two = get_theme_mod('social_media_icon_two');
											$social_media_two_user_id = get_theme_mod('social_media_two_user_id');
											array_push($social_options, $social_media_icon_two, $social_media_two_user_id);
										}

										if (get_theme_mod('social_media_icon_three') != '')
										{
											$social_media_icon_three = get_theme_mod('social_media_icon_three');
											$social_media_three_user_id = get_theme_mod('social_media_three_user_id');
											array_push($social_options, $social_media_icon_three, $social_media_three_user_id);
										}

										if (get_theme_mod('social_media_icon_four') != '')
										{
											$social_media_icon_four = get_theme_mod('social_media_icon_four');
											$social_media_four_user_id = get_theme_mod('social_media_four_user_id');
											array_push($social_options, $social_media_icon_four, $social_media_four_user_id);
										}

										if (get_theme_mod('social_media_icon_five') != '')
										{
											$social_media_icon_five = get_theme_mod('social_media_icon_five');
											$social_media_five_user_id = get_theme_mod('social_media_five_user_id');
											array_push($social_options, $social_media_icon_five, $social_media_five_user_id);
										}

										if (get_theme_mod('social_media_icon_six') != '')
										{
											$social_media_icon_six = get_theme_mod('social_media_icon_six');
											$social_media_six_user_id = get_theme_mod('social_media_six_user_id');
											array_push($social_options, $social_media_icon_six, $social_media_six_user_id);
										}

										if (get_theme_mod('social_media_icon_seven') != '')
										{
											$social_media_icon_seven = get_theme_mod('social_media_icon_seven');
											$social_media_seven_user_id = get_theme_mod('social_media_seven_user_id');
											array_push($social_options, $social_media_icon_seven, $social_media_seven_user_id);
										}

										if (get_theme_mod('social_media_icon_eight') != '')
										{
											$social_media_icon_eight = get_theme_mod('social_media_icon_eight');
											$social_media_eight_user_id = get_theme_mod('social_media_eight_user_id');
											array_push($social_options, $social_media_icon_eight, $social_media_eight_user_id);
										}

										if (get_theme_mod('social_media_icon_nine') != '')
										{
											$social_media_icon_nine = get_theme_mod('social_media_icon_nine');
											$social_media_nine_user_id = get_theme_mod('social_media_nine_user_id');
											array_push($social_options, $social_media_icon_nine, $social_media_nine_user_id);
										}

										if (get_theme_mod('social_media_icon_ten') != '')
										{
											$social_media_icon_ten = get_theme_mod('social_media_icon_ten');
											$social_media_ten_user_id = get_theme_mod('social_media_ten_user_id');
											array_push($social_options, $social_media_icon_ten, $social_media_ten_user_id);
										}
										
										foreach ( $social_options as $key => $value ) 
										{
											if ( $value ) 
											{ 
											?>
												<a href="<?php echo $value; ?>" title="<?php echo $key; ?>" target="_blank">
													<i class="fa fa-<?php echo $value; ?>"></i>
												</a>
											<?php 
											}
										} 
										?>
								</div><!-- .social-icons -->
						</div>
					</div><!-- .row -->

				</div>
		</footer>


    </div><!-- container -->
</div><!-- end wrapper -->
<?php wp_footer(); ?>

</body>
</html>