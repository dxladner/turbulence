<?php
/**
 * Turbulence functions and definitions
 *
 * @package Turbulence
 */

/*
|--------------------------------------------------------------------------
| CONSTANTS
|--------------------------------------------------------------------------
*/
if( !defined( 'THEME_DIRECTORY' ) ) {
    define( 'THEME_DIRECTORY', get_template_directory_uri() );
}
if( !defined( 'THEME_DIR_PATH' ) ) {
    define( 'THEME_DIR_PATH', get_template_directory() );
}
if(!defined('THEME_DIR_FILE')) {
    define('THEME_DIR_FILE', __FILE__);
}

include_once( dirname( __FILE__ ) . '/kirki/kirki.php' );

function prefix_theme_updater() {
    require( get_template_directory() . '/updater/theme-updater.php' );
}
add_action( 'after_setup_theme', 'prefix_theme_updater' );

define('VERSION', '1.0.0');


$turbulence_license_key_status = get_option('turbulence_license_key_status');

if ( $turbulence_license_key_status != 'valid' )
{
	function rua_activate_turbulence_license_notice() {
    ?>
    <div class="notice notice-error is-dismissible">
        <p><?php _e( '<h5>TURBULENCE: Please activate your license in order for get all of the theme customizer settings to show up and your theme to work correctly.!</h5>
                    <p>Feel free to close the notice but until you enter your License key and activate it, all of the theme customizer settings for the plugin will not show up. 
                    Do not forget!
                    Also, you can just activate another theme until you recieve your license to stop the admin notices.
                    You should have received your license in an email we sent. Look under the settings menu and you should see a link to the license page.
                    If you did not recieve an email with your license or having trouble with your license getting actiavted, please send us a support ticket at
                    our website <a target="_blank" href="https://hyperdrivedesigns.com/js-support-ticket-controlpanel/">HERE</a></p>', 'turbulence' ); ?></p>
    </div>
    <?php
	}
    add_action( 'admin_notices', 'rua_activate_turbulence_license_notice' );
}
else if ( $turbulence_license_key_status == 'valid' )
{
	if ( class_exists( 'Kirki' ) ) {
		/**
		 * Add the configuration.
		 * This way all the fields using the 'kirki_demo' ID
		 * will inherit these options
		 */
		Kirki::add_config( 'turbulence', array(
			'capability'    => 'edit_theme_options',
			'option_type'   => 'theme_mod',
		) );

		require_once( trailingslashit( get_template_directory() ) . 'controls-sections/blog-controls.php' );

	    require_once( trailingslashit( get_template_directory() ) . 'controls-sections/contact-google-map-controls.php' );
	    
	    require_once( trailingslashit( get_template_directory() ) . 'controls-sections/faq-controls.php' );
	     
	    require_once( trailingslashit( get_template_directory() ) . 'controls-sections/footer-controls.php' );

	    require_once( trailingslashit( get_template_directory() ) . 'controls-sections/general-controls.php' );

	    require_once( trailingslashit( get_template_directory() ) . 'controls-sections/header-controls.php' );

		require_once( trailingslashit( get_template_directory() ) . 'controls-sections/homepage-controls.php' );

		require_once( trailingslashit( get_template_directory() ) . 'controls-sections/landing-page-controls.php' );

	    require_once( trailingslashit( get_template_directory() ) . 'controls-sections/portfolio-controls.php' );

	    require_once( trailingslashit( get_template_directory() ) . 'controls-sections/services-controls.php' );
	    
	    require_once( trailingslashit( get_template_directory() ) . 'controls-sections/social-media-controls.php' );

	    require_once( trailingslashit( get_template_directory() ) . 'controls-sections/team-controls.php' );
	}

}


if ( ! isset( $content_width ) ) $content_width = 900;

if ( ! function_exists( 'turbulence_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */


function turbulence_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Turbulence, use a find and replace
	 * to change 'turbulence' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'turbulence', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'turbulence' ),
	) );
	
	// footer nav menu
	register_nav_menus( array(
		'footer-menu' => __( 'Footer Menu', 'turbulence' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See http://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'quote', 'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'turbulence_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
	
	add_theme_support( "title-tag" ); 
	
}
endif; // turbulence_setup

add_action( 'after_setup_theme', 'turbulence_setup' );



function turbulence_theme_customizer( $wp_customize ) {
		// remove theme header image
        $wp_customize->remove_control('header_image');
        $wp_customize->remove_panel('header_image');
        $wp_customize->remove_section('header_image');
        $wp_customize->remove_setting('header_image');
        // remove theme background image
        $wp_customize->remove_control('background_image');
        $wp_customize->remove_panel('background_image');
        $wp_customize->remove_section('background_image');
        $wp_customize->remove_setting('background_image');
        // remove theme colors picker
        $wp_customize->remove_control('colors');
        $wp_customize->remove_panel('colors');
        $wp_customize->remove_section('colors');
        $wp_customize->remove_setting('colors');

		// turbulence favicon control
		$wp_customize->add_setting( 'turbulence_favicon', array(
            'default'           => '',
            'type'              => 'theme_mod',
            'capability'        => 'edit_theme_options',
            'transport'         => '',
        ) );
        $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'turbulence_favicon', array(
                'label'    		=> __( 'Favicon', 'turbulence' ),
                'section'  		=> 'general_settings',
                'settings' 		=> 'turbulence_favicon',
                'description'   => __('Use this field to upload your custom favicon. (Recommended 200 px x 40 px)', 'turbulence'),
                'priority'     => 15,
                )
            )
        );
        // turbulence logo control
        $wp_customize->add_setting( 'turbulence_logo', array(
            'default'           => '',
            'type'              => 'theme_mod',
            'capability'        => 'edit_theme_options',
            'transport'         => '',
        ) );
        $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'turbulence_logo', array(
                'label'    => __( 'Logo', 'turbulence' ),
                'section'  => 'turbulence_header_section',
                'settings' => 'turbulence_logo',
                'description'   => __('Use this field to upload your custom logo for use in the theme header.(Recommended 200 px x 40 px)', 'turbulence'),
				'priority'     => 15,
                )
            )
        );
         // homepage featured image upload control
        $wp_customize->add_setting( 'homepage_featured_image', array(
            'default'           => '',
            'type'              => 'theme_mod',
            'capability'        => 'edit_theme_options',
            'transport'         => '',
        ) );
        $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'homepage_featured_image', array(
                'label'         => __( 'Home Page Featured Image', 'turbulence' ),
                'description'   => __( 'Use this field to upload your background image for use in the theme header.', 'turbulence' ),
                'section'       => 'homepage_featured_image_section',
                'settings'      => 'homepage_featured_image',
                )
            )
        );
         // landing page featured image upload control
        $wp_customize->add_setting( 'landing_page_featured_image', array(
            'default'           => '',
            'type'              => 'theme_mod',
            'capability'        => 'edit_theme_options',
            'transport'         => '',
            'sanitize_callback' => 'sanitize_text_field',
        ) );
        $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'landing_page_featured_image', array(
                'label'    		=> __( 'Landing Page Featured Image', 'turbulence' ),
                'section'  		=> 'landing_page_header_section',
                'settings' 		=> 'landing_page_featured_image',
                'description'   => __('Use this field to upload your Landing Page Featured Image.', 'turbulence'),
                )
            )
        );
}

add_action( 'customize_register', 'turbulence_theme_customizer' );

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function turbulence_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'turbulence' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	) );
	
	register_sidebar( array(
		'name'          => __( 'Sidebar Left', 'turbulence' ),
		'id'            => 'sidebar-left',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	) );
	
		register_sidebar( array(
		'name'          => __( 'Contact', 'turbulence' ),
		'id'            => 'contact',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	) );

	register_sidebar(array (
		'name'		=> __('Footer One', 'turbulence'),
		'id'		=> 'footer-one-widget-area',
		'description'	=> __('The first footer widget area', 'dir'),
		'before_widget'	=> '',
		'after_widget'	=> '',
		) 
	);
	register_sidebar(array (
		'name'		=> __('Footer Two', 'turbulence'),
		'id'		=> 'footer-two-widget-area',
		'description'	=> __('The second footer widget area', 'dir'),
		'before_widget'	=> '',
		'after_widget'	=> '',
		) 
	);
	register_sidebar(array (
		'name'		=> __('Footer Three', 'turbulence'),
		'id'		=> 'footer-three-widget-area',
		'description'	=> __('The third footer widget area', 'dir'),
		'before_widget'	=> '',
		'after_widget'	=> '',
		) 
	);

}
add_action( 'widgets_init', 'turbulence_widgets_init' );

/**
* Enqueue scripts and styles.
*/
function turbulence_scripts() {
    
        wp_enqueue_style( 'bootstrap-styles', get_template_directory_uri() . '/css/'. get_theme_mod('bootstrap_themes', 'bootstrap.min.css'), array(), '3.3.6', 'all' );
 
		wp_enqueue_style( 'font-awesome', get_template_directory_uri() . '/css/font-awesome.min.css', array(), '4.5	.0', 'all' );

		wp_enqueue_style( 'font-awesome', get_template_directory_uri() . '/css/turbulence-shortcodes.css', array(), '4.2.0', 'all' );
	
		wp_enqueue_style( 'turbulence-shortcodes', get_template_directory_uri() . '/css/turbulence-shortcodes.css', array(), '4.2.0', 'all' );
        
        wp_enqueue_style( 'turbulence-style', get_stylesheet_uri(), array( 'dashicons' ), '1.0' );
	
		wp_enqueue_style( 'main-css', get_template_directory_uri() . '/css/main.css', array(), '1.0.0', 'all' );
         
        wp_enqueue_script( 'bootstrap-js', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'), '3.2.0', true );
                 
        wp_enqueue_script( 'isotope-js', get_template_directory_uri() . '/js/isotope.pkgd.min.js', array('jquery'), '2.0.1', true );
 
        wp_enqueue_script( 'imagesloaded-js', get_template_directory_uri() . '/js/imagesloaded.pkgd.min.js', array('jquery'), '3.1.8', true );
        
        wp_enqueue_script( 'circles-js', get_template_directory_uri() . '/js/circles.min.js', array('jquery'), '3.2.0', true );

        wp_enqueue_script( 'main-js', get_template_directory_uri() . '/js/main.js', array('jquery'), '3.2.0', true );
	
		wp_enqueue_script( 'turbulence-shortcodes-js', get_template_directory_uri() . '/js/turbulence-shortcodes.js', array('jquery'), '3.2.0', true );
	
        if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }
}
add_action( 'wp_enqueue_scripts', 'turbulence_scripts' );

function turbulence_admin_styles() {
	wp_enqueue_style( 'admin-css', get_template_directory_uri() . '/css/admin-css.css' );
}
add_action( 'admin_enqueue_scripts', 'turbulence_admin_styles' );

if(!function_exists('ie_scripts')) {
	function ie_scripts() {
		echo '<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->';
    	echo '<!-- WARNING: Respond.js does not work if you view the page via file:// -->';
    	echo '<!--[if lt IE 9]>';
      	echo '<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>';
      	echo '<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>';
    	echo '<![endif]-->';
	}
}
add_action('wp_head', 'ie_scripts');

// CREATE CUSTOM POST TYPES
add_action( 'init', 'create_team_members' );

function create_team_members() 
{
	register_post_type( 'team',
		array(
		'labels' => array(
    		'name' 					=> __( 'Team', 'turbulence' ),
    		'singular_name' 		=> __( 'Team Member', 'turbulence' ),
    		'add_new' 				=> __( 'Add New', 'turbulence' ),
    		'add_new_item' 			=> __( 'Add New Team Member', 'turbulence' ),
    		'edit' 					=> __( 'Edit', 'turbulence' ),
    		'edit_item' 			=> __( 'Edit Team Member', 'turbulence' ),
    		'new_item' 				=> __( 'New Team Member', 'turbulence' ),
    		'view' 					=> __( 'View', 'turbulence' ),
    		'view_item' 			=> __( 'View Team Member', 'turbulence' ),
    		'search_items' 			=> __( 'Search Team Member', 'turbulence' ),
    		'not_found' 			=> __( 'No Team Member', 'turbulence' ),
    		'not_found_in_trash' 	=>
    		'No Team Member found in Trash'
    		),
		'public' => true,
		'menu_position' => 15,
		'menu_icon' => 'dashicons-[groups]',
		'supports' => array(
		    'title', 
		    'editor', 
		    'thumbnail',
		    'revisions'
		    ),
		'show_admin_column'     => true,
		'public'                => true,
		'show_ui'               => true,
		'register_meta_box_cb'  => 'team_meta_boxes',
		)
	);
}

function team_meta_boxes() {
    add_meta_box('team_member_meta', __( 'Team Member Info', 'turbulence' ), 'team_member_info_meta_box', 'team', 'normal', 'high');
}

function team_member_info_meta_box() {
    global $post;
    if(function_exists('wp_nonce_field'))
        wp_nonce_field('team_member_info_nonce', '_team_member_info_nonce');
    ?>
        <p><label for="job_title"><?php __('Job Title', 'tubulence'); ?></label>
        <input type="text" name="job_title" id="job_title" value="<?php echo esc_html(get_post_meta(get_the_ID(), 'job_title', true), 1); ?>" />
        </p>
    
        <p><label for="team_member_email"><?php __('Email', 'tubulence'); ?></label>
        <input type="text" name="team_member_email" id="team_member_email" value="<?php echo esc_html(get_post_meta(get_the_ID(), 'team_member_email', true), 1); ?>" />
        </p>
        
        <p><label for="team_member_facebook"><?php __('Facebook', 'tubulence'); ?></label>
        <input type="text" name="team_member_facebook" id="team_member_facebook" value="<?php echo esc_html(get_post_meta(get_the_ID(), 'team_member_facebook', true), 1); ?>" />
        </p>
        
        <p><label for="team_member_twitter"><?php __('Twitter', 'tubulence'); ?></label>
        <input type="text" name="team_member_twitter" id="team_member_twitter" value="<?php echo esc_html(get_post_meta(get_the_ID(), 'team_member_twitter', true), 1); ?>" />
        </p>
        
        <p><label for="team_member_git_bitbucket"><?php __('Git/Bitbucket', 'tubulence'); ?></label>
        <input type="text" name="team_member_git_bitbucket" id="team_member_git_bitbucket" value="<?php echo esc_html(get_post_meta(get_the_ID(), 'team_member_git_bitbucket', true), 1); ?>" />
        </p>
    <?php
}

add_action( 'init', 'create_team_taxonomy' );
function create_team_taxonomy() {
	register_taxonomy(
		'skills',
		'team',
		array(
			'labels' => array(
			    'name'                          => __( 'Skills', 'turbulence' ),
			    'singular_name'                 => __( 'Skill', 'turbulence' ),
			    'seach_items'                   => __( 'Search Skills', 'turbulence' ),
			    'all_items'                     => __( 'All Skills', 'turbulence' ),
			    'edit_item'                     => __( 'Edit Skills', 'turbulence' ),
			    'update_item'                   => __( 'Update Skills', 'turbulence' ),
			    'add_new_item'                  => __( 'Add New Skill', 'turbulence' ),
			    'new_item_name'                 => __( 'New Skill Name', 'turbulence' ),
			    'menu_name'                     => __( 'Skills', 'turbulence' ),
			    'seperate_items_with_commas'    => __( 'Seperate skills with commas', 'turbulence' ),
			    'add_or_remove_items'           => __( 'Add or remove skills', 'turbulence' ),
			    ),
			    
			'rewrite'               => array( 'slug' => 'skills' ),
			'hierarchical'          => false,
		)
	);
}

$post_type = 'team';
function team_text_after_title( $post_type ) {
    $screen = get_current_screen();
    $edit_post_type = $screen->post_type;
    if ( $edit_post_type != 'team' )
        return;
    ?>
    <div class="after-title-help postbox">
        <h3>How to create a Team Member</h3>
        <div class="inside">
            <p>The Post Title is the Team Members Name.<br />
            The Editor Screen is for writing the Team Members Quick Bio<br/>
            The Featured Image is for Team Members Head Shot<br/>
            The Team Members Custom Meta Box is for Team Members Information<br/>
            The Job Title Field is for the Team Members Job Title<br/>
            The Email Field is for the Team Members Email Address<br/>
            The Facebook Field is for the Team Members Facebook profile<br/>
            The Twitter Field is for the Team Members Twitter profile<br/>
            The Git or BitBucket Field is for the Team Members Repo profile<br/>
            </p>
        </div><!-- .inside -->
    </div><!-- .postbox -->
<?php }
add_action( 'edit_form_after_title', 'team_text_after_title' );

add_action('save_post', 'team_save_extras');

global $post_id;
function team_save_extras( $post_id ) {
    global $post;   
    //skip auto save
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
        return $post_id;
    }
	global $post_type;
    //check for you post type only
    if( $post_type == "team" ) {
        if( isset($_POST['job_title']) ) { update_post_meta( $post->ID, 'job_title', $_POST['job_title'] );}
        if( isset($_POST['team_member_email']) ) { update_post_meta( $post->ID, 'team_member_email', $_POST['team_member_email'] );}
		if( isset($_POST['team_member_facebook']) ) { update_post_meta( $post->ID, 'team_member_facebook', $_POST['team_member_facebook'] );}
		if( isset($_POST['team_member_twitter']) ) { update_post_meta( $post->ID, 'team_member_twitter', $_POST['team_member_twitter'] );}
		if( isset($_POST['team_member_git_bitbucket']) ) { update_post_meta( $post->ID, 'team_member_git_bitbucket', $_POST['team_member_git_bitbucket'] );}

    }
} 

// Add to admin_init function
add_filter('manage_edit-team_columns', 'add_new_team_columns');

function add_new_team_columns($team_columns) {
    $new_columns['cb'] = '<input type="checkbox" />';
    $new_columns['title'] = __('Team Member Name', 'column name');
    $new_columns['bio'] = __('Bio', 'column name');
    $new_columns['job_title'] = __('Job Title', 'column name');
    $new_columns['team_member_email'] = __('Email', 'column name');
    $new_columns['team_member_facebook'] = __('Facebook', 'column name');
    $new_columns['team_member_twitter'] = __('Twitter', 'column name');
    $new_columns['team_member_git_bitbucket'] = __('Git/Bitbucket', 'column name');
    $new_columns['skills'] = __('Skill Tags', 'column_name');
 
    return $new_columns;
}


add_action( 'manage_posts_custom_column', 'populate_team_columns' );

function populate_team_columns( $column) 
{
   
	if ( 'job_title' == $column ) 
	{
        $job_title = esc_html(get_post_meta( get_the_ID(), 'job_title', true ) );
        echo $job_title;
    }
    if ( 'bio' == $column ) 
	{
        $bio = get_the_content();
        echo substr($bio, 0, 70);
    }
    elseif ('team_member_email' == $column) 
    {
    	$team_member_email = esc_html(get_post_meta( get_the_ID(), 'team_member_email', true ) );
        echo $team_member_email;
    }
    elseif ('team_member_facebook' == $column) 
    {
    	$team_member_facebook = esc_html(get_post_meta( get_the_ID(), 'team_member_facebook', true ) );
        echo $team_member_facebook;
    }
    elseif ('team_member_twitter' == $column) 
    {
    	$team_member_twitter = esc_html(get_post_meta( get_the_ID(), 'team_member_twitter', true ) );
        echo $team_member_twitter;
    }
    elseif ('team_member_git_bitbucket' == $column) 
    {
    	$team_member_git_bitbucket = esc_html(get_post_meta( get_the_ID(), 'team_member_git_bitbucket', true ) );
        echo $team_member_git_bitbucket;
    }
    elseif ('skills' == $column) 
    {
    	$skills = custom_taxonomies_terms_links();
        echo $skills;
    }

}

// get taxonomies terms links
function custom_taxonomies_terms_links(){
     global $post;
  // get post by post id
  $post = get_post($post->ID);

  // get post type by post
  $post_type = $post->post_type;

  // get post type taxonomies
  $taxonomies = get_object_taxonomies( $post_type, 'objects' );

  $out = array();
  foreach ( $taxonomies as $taxonomy_slug => $taxonomy ){

    // get the terms related to post
    $terms = get_the_terms( $post->ID, $taxonomy_slug );

    if ( !empty( $terms ) ) {
      //$out[] = "<h2>" . $taxonomy->label . "</h2>\n<ul>";
      foreach ( $terms as $term ) {
        $out[] =
          ' <ul> <li><a href="'
        .    get_term_link( $term->slug, $taxonomy_slug ) .'">'
        .    $term->name
        . "</a></li>\n";
      }
      $out[] = "</ul>\n";
    }
  }

  return implode('', $out );
}


/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Load Bootstrap Menu.
 */
require get_template_directory() . '/inc/bootstrap-walker.php';

/**
 * Comments Callback.
 */
require get_template_directory() . '/inc/comments-callback.php';

/**
 * Author Meta.
 */
require get_template_directory() . '/inc/author-meta.php';

/**
 * Search Results - Highlight.
 */
require get_template_directory() . '/inc/search-highlight.php';

/**
 * Custom Post Types
 */
require get_template_directory() . '/inc/post-types/CPT.php';

//Portfolio Custom Post Type
require get_template_directory() . '/inc/post-types/register-portfolio.php';

//Portfolio Custom Post Type
require get_template_directory() . '/includes/turbulence-shortcodes.php';

/**
 * Theme Options - Custom CSS.
 */
require get_template_directory() . '/inc/custom-css.php';

/**
 * Shortcodes.
 */
function my_add_mce_button() {
	// check user permissions
	if ( !current_user_can( 'edit_posts' ) && !current_user_can( 'edit_pages' ) ) {
		return;
	}
	// check if WYSIWYG is enabled
	if ( 'true' == get_user_option( 'rich_editing' ) ) {
		add_filter( 'mce_external_plugins', 'my_add_tinymce_plugin' );
		add_filter( 'mce_buttons', 'my_register_mce_button' );
	}
}
add_action('admin_head', 'my_add_mce_button');

// Declare script for new button
function my_add_tinymce_plugin( $plugin_array ) {
	$plugin_array['my_mce_button'] = get_template_directory_uri() .'/js/mce-button.js';
	return $plugin_array;
}

// Register new button in the editor
function my_register_mce_button( $buttons ) {
	array_push( $buttons, 'my_mce_button' );
	return $buttons;
}
