<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package turbulence
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,700|Merriweather:400,400italic,700italic" rel="stylesheet" type="text/css">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<?php if (get_theme_mod('turbulence_favicon') != '') : ?>
	<?php $favicon = get_theme_mod('turbulence_favicon'); ?>
	<link rel="icon" type="image/png" href="<?php echo $favicon; ?>" />
<?php endif; ?>
<?php wp_head(); ?>
</head>
					
<body <?php body_class(); ?>>
<div class="wrapper"><!-- start wrapper -->

	<header id="masthead" class="site-header" role="banner">
	<?php 
	$fixed = (get_theme_mod('turbulence_navbar_fixed') == '1' ? 'navbar-fixed-top' : 'navbar-static-top');
	$inverse = (get_theme_mod('turbulence_navbar_inverse') == '1' ? 'navbar-inverse' : 'navbar-default');
	?>
	<nav role="navigation">
	<div class="navbar <?php echo $fixed; ?> <?php echo $inverse; ?> ">
		
			<div class="headercontainer">
				<!-- .navbar-toggle is used as the toggle for collapsed navbar content -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
                    <?php $logo = get_theme_mod('turbulence_logo'); ?>
 					<div id="logo">
						<?php if($logo !== '') { ?>
							<a href="<?php echo esc_url( home_url() ) ?>/" title="<?php bloginfo( 'name' ) ?>" rel="homepage"><img src="<?php echo $logo ?>" alt="<?php bloginfo( 'name' ) ?>"></a>
						<?php } else { ?>
							<a class="navbar-brand" href="<?php echo esc_url( home_url() ) ?>/" title="<?php bloginfo( 'name' ) ?>" rel="homepage"><?php bloginfo( 'name' ) ?></a>
						<?php } ?>
					</div>
            	</div>

				<div class="navbar-collapse collapse navbar-responsive-collapse">
					<?php

					$args = array(
						'theme_location' => 'primary',
						'depth'      => 2,
						'container'  => false,
						'menu_class'     => 'nav navbar-nav navbar-right',
						'walker'     => new Bootstrap_Walker_Nav_Menu()
						);

					if (has_nav_menu('primary')) {
						wp_nav_menu($args);
					}

					?>

				</div>
			</div>
		</div>           
	</nav>
	</header><!-- #masthead -->    