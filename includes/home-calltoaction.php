<?php
/**
 * File used for homepage call to action module
 *
 * @package turbulence
 */
?>
<?php
$call_to_action_switch = get_theme_mod('homepage_call_to_section_enable'); 
$call_to_action_header = get_theme_mod('homepage_call_to_action_header');
$call_to_action_btn_text = get_theme_mod('homepage_call_to_action_button_text');
$call_to_action_btn_size = 'btn-'.get_theme_mod('homepage_call_to_action_button_size');
$call_to_action_btn_color = 'btn-'.get_theme_mod('call_to_action_button_color');
$call_to_action_btn_url = get_theme_mod('homepage_call_to_action_button_url');
?>


<?php if($call_to_action_switch == 'enable') 
{
?>
</div><!-- end container -->
    <div class="row paddingBottom">
        <div class="col-md-12">
            <div id="callToAction">
                <div class="container centered">
                    <h2><?php echo $call_to_action_header; ?></h2>
                    <br>
                    <p><a href="<?php echo $call_to_action_btn_url; ?>" target="_self" class="btn <?php echo $call_to_action_btn_color .' '. $call_to_action_btn_size; ?>" role="button"><?php echo $call_to_action_btn_text; ?></a></p>
                    <br>
                </div>
            </div>
        </div>
    </div>
<div class="container">
<?php
}
?>
    