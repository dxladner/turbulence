<?php
/**
 * File used for homepage hero content module
 *
 * @package WordPress
 */
?>


<div class="jumbotron">
	<div class="row">
		<div class="col-lg-6">
			<?php if(get_theme_mod('homepage_post_featured_heading') != '') { ?>
			<h1> <?php echo get_theme_mod('homepage_post_featured_heading'); ?> </h1>
			<?php } ?>

			<?php if(get_theme_mod('homepage_post_featured_content') != '') { ?>
			<p><?php echo get_theme_mod('homepage_post_featured_content'); ?> </p>
			<?php } ?>

			<?php 
			$btn_text = get_theme_mod('homepage_post_featured_button_text', '');
			$btn_size = 'btn-'.get_theme_mod('homepage_hero_post_featured_button_size', '');
			$btn_color = 'btn-'.get_theme_mod('homepage_hero_post_featured_button_color', '');
			$btn_url = get_theme_mod('homepage_post_featured_button_url', '');

			if (get_theme_mod('homepage_post_featured_button_full_width_block') == 'enable' ){
				$btn_block = "btn-block";
			}

			?>

			<?php if (get_theme_mod('homepage_post_featured_button_enable') == 'enable' ){	?>	      
		    <p><a href="<?php echo $btn_url; ?>" target="_self" class="btn <?php echo $btn_color .' '. $btn_size .' '. $btn_block; ?>" role="button"><?php echo $btn_text; ?></a></p>
		    <?php } ?>

		</div>
		<div class="col-lg-6">
			<?php if(get_theme_mod('homepage_hero_post_right_featured_content') != '') { 
				echo get_theme_mod('homepage_hero_post_right_featured_content');
			} ?>
		</div>
	</div>  
</div>