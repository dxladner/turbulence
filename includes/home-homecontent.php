<?php
/**
 * File used for homepage static page content module
 *
 * @package WordPress
 */
?>
<?php 
$numberOfPosts = get_theme_mod('homepage_number_of_posts');
			// the query
			$homepost = new WP_Query('posts_per_page='.$numberOfPosts.'&ignore_sticky_posts=1'); ?>


			<?php while ( $homepost->have_posts() ) : $homepost->the_post(); ?>

			<?php global $more; $more = 0; ?>

			<?php
					/* Include the Post-Format-specific template for the content.
					 * If you want to override this in a child theme, then include a file
					 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
					 */
					get_template_part( 'content-excerpt', get_post_format() );
				?>

			<?php endwhile; ?>