<?php
/**
 * File used for homepage widget module
 *
 * @package WordPress
 */
?>

<div class="home-widgets">
	<div class="row">
		<div class="col-lg-4 col-md-4">
	    <?php $fa_icon_one = get_theme_mod('homepage_widgets_one_icon', ''); ?>
		    
			<p class="widgets_icon"><i class="fa fa-<?php echo $fa_icon_one; ?> fa-4x"></i></p>
			<h4 class="widgets_header">
			<?php if (get_theme_mod('homepage_widget_one_header') != '') { ?>
				<?php echo get_theme_mod('homepage_widget_one_header'); ?>
			<?php } ?>
			</h4>
			<p class="widgets_content">
			<?php if (get_theme_mod('homepage_widget_one_content') != '') { ?>
				<?php echo get_theme_mod('homepage_widget_one_content'); ?>
			<?php } ?>
			</p>
		</div>

		<div class="col-lg-4 col-md-4">
			<?php $fa_icon_two = get_theme_mod('homepage_widgets_two_icon', ''); ?>
		    
			<p class="widgets_icon"><i class="fa fa-<?php echo $fa_icon_two; ?> fa-4x"></i></p>
			<h4 class="widgets_header">
			<?php if (get_theme_mod('homepage_widget_two_header') != '') { ?>
				<?php echo get_theme_mod('homepage_widget_two_header'); ?>
			<?php } ?>
			</h4>
			<p class="widgets_content">
			<?php if (get_theme_mod('homepage_widget_two_content') != '') { ?>
				<?php echo get_theme_mod('homepage_widget_two_content'); ?>
			<?php } ?>
			</p>
		</div>

		<div class="col-lg-4 col-md-4">
			<?php $fa_icon_three = get_theme_mod('homepage_widgets_three_icon', ''); ?>
		    
			<p class="widgets_icon"><i class="fa fa-<?php echo $fa_icon_three; ?> fa-4x"></i></p>
			<h4 class="widgets_header">
			<?php if (get_theme_mod('homepage_widget_three_header') != '') { ?>
				<?php echo get_theme_mod('homepage_widget_three_header'); ?>
			<?php } ?>
			</h4>
			<p class="widgets_content"><?php if (get_theme_mod('homepage_widget_three_content') != '') { ?>
				<?php echo get_theme_mod('homepage_widget_three_content'); ?>
			<?php } ?>
			</p>
		</div>

	</div><!--row-->
</div>