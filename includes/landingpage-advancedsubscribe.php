<?php
if(isset($_POST['submitted'])) 
{
    // Name
    if(trim($_POST['contactName']) === '') 
    {
        $nameError = true;
        $hasError = true;
    } 
    else 
    {
        $name = trim($_POST['contactName']);
    }

    // Email
    if(trim($_POST['email']) === '')  
    {
        $emailError = true;
        $hasError = true;
    } 
    else if (!preg_match("/^[[:alnum:]][a-z0-9_.-]*@[a-z0-9.-]+\.[a-z]{2,4}$/i", trim($_POST['email']))) 
    {
        $emailError = true;
        $hasError = true;
    } 
    else 
    {
        $email = trim($_POST['email']);
    }

    // Phone
    
    if (!preg_match("/^(\d[\s-]?)?[\(\[\s-]{0,2}?\d{3}[\)\]\s-]{0,2}?\d{3}[\s-]?\d{4}$/i", trim($_POST['phone']))) 
    {
        if(trim($_POST['phone']) === '')
        {
            $phone = trim($_POST['phone']);
        }
        else
        {
            $phoneError = true;
            $hasError = true;
        }
    }
    else
    {
        $phone = trim($_POST['phone']);
    }
    
   
   
    // Website Address
    if (!preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i", trim($_POST['website'])))
    {
        if(trim($_POST['website']) === '')
        {
            if(function_exists('stripslashes')) 
            {
                $website = stripslashes(trim($_POST['website']));
            } 
            else 
            {
                $website = trim($_POST['website']);
            }
        }
        else
        {
            $websiteError = true;
            $hasError = true;
        }   
    }
    else
    {
        $website = trim($_POST['website']);
    }
    
    // Company Name
    if(function_exists('stripslashes')) 
    {
        $companyName = stripslashes(trim($_POST['companyName']));
    } 
    else 
    {
        $companyName = trim($_POST['companyName']);
    }

    // Message
    if(function_exists('stripslashes')) 
    {
        $comments = stripslashes(trim($_POST['comments']));
    } 
    else 
    {
        $comments = trim($_POST['comments']);
    }
    

    if(!isset($hasError)) {
        $emailTo = get_option('admin_email');
        if (!isset($emailTo) || ($emailTo == '') ){
            $emailTo = get_option('admin_email');
        }
        $subject = __('From ','turbulence').$name;
        $body = __('Name: ','turbulence').$name."\n".__('Phone: ','turbulence').$phone."\n".__('Email: ','turbulence').$email."\n".__('Website: ','turbulence').$website."\n".__('Company Name: ','turbulence').$companyName."\n".__('Comments: ','turbulence').$comments;
        $headers = __('From: ','turbulence') .$name. ' <'.$emailTo.'>' . "\r\n" . __('Reply-To:','turbulence') .$name. '<'.$email.'>';

        wp_mail($emailTo, $subject, $body, $headers);
        $emailSent = true;
    }

}
	$lp_btn_text = get_theme_mod('landing_page_button_text', '');
	$lp_btn_size = 'btn-'.get_theme_mod('landing_page_button_size', '');
	$lp_btn_color = 'btn-'.get_theme_mod('landing_page_button_color', '');
?>

<div class="container advancedSubscribe">
    <div class="row">
        <?php if(isset($emailSent) && $emailSent == true) { ?>
                <div class="alert alert-success" role="alert">
                    <p><?php _e('Thank you for subscribing, your form was submitted successfully.', 'turbulence'); ?></p>
                </div>
            <?php } else { ?>

                <?php if(isset($hasError) || isset($captchaError)) { ?>
                    <div class="alert alert-danger alert-dismissible" role="alert">
					  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					  <strong><?php _e('Error!', 'turbulence'); ?></strong> <?php _e('Please try again.', 'turbulence'); ?>
					</div>
                <?php } ?>
                <div class="col-md-3">
                </div>
                <div class="col-md-6">
		        <form action="<?php the_permalink(); ?>" id="advancedSubscribe" method="post" role="form">
		            <div class="form-group <?php if(isset($nameError)) { echo "has-error has-feedback"; }?>">
		                    <input class="form-control" type="text" name="contactName" id="contactName" value="" placeholder="Name (Required)" />
		                    <?php if(isset($nameError)) { ?>
		                        <span class="glyphicon glyphicon-remove form-control-feedback"></span>
		                    <?php } ?>
		               </div>
		               <div class="form-group <?php if(isset($emailError)) { echo "has-error has-feedback"; }?>">
		                    <input class="form-control" type="text" name="email" id="email" value="" placeholder="Email Address (Required)" />
		                    <?php if(isset($emailError)) { ?>
		                        <span class="glyphicon glyphicon-remove form-control-feedback"></span>
		                    <?php } ?>
		               </div>
		               <div class="form-group <?php if(isset($phoneError)) { echo "has-error has-feedback"; }?>">
		                    <input class="form-control" type="text" name="phone" id="phone" value="" placeholder="Phone Number" />
		                    <?php if(isset($phoneError)) { ?>
		                        <span class="glyphicon glyphicon-remove form-control-feedback"></span>
		                    <?php } ?>
		               </div>
		               <div class="form-group <?php if(isset($websiteError)) { echo "has-error has-feedback"; }?>">
		                    <input class="form-control" type="text" name="website" id="website" value="" placeholder="Website Address" />
		                    <?php if(isset($websiteError)) { ?>
		                        <span class="glyphicon glyphicon-remove form-control-feedback"></span>
		                    <?php } ?>
		               </div>
		               <div class="form-group <?php if(isset($companyNameError)) { echo "has-error has-feedback"; }?>">
		                    <input class="form-control" type="text" name="companyName" id="companyName" value="" placeholder="Company Name" />
		                    <?php if(isset($companyNameError)) { ?>
		                        <span class="glyphicon glyphicon-remove form-control-feedback"></span>
		                    <?php } ?>
		               </div>
		                <div class="form-group <?php if(isset($commentError)) { echo "has-error has-feedback"; }?>">
		                    <textarea class="form-control" name="comments" id="comments" rows="10" cols="20" placeholder="Message"></textarea>
		                     <?php if(isset($commentError)) { ?>
		                        <span class="glyphicon glyphicon-remove form-control-feedback"></span>
		                    <?php } ?>
		               </div>
		               <div class="form-actions">
		                  <button type="submit" class="btn <?php echo $lp_btn_color .' '. $lp_btn_size; ?>"><?php echo $lp_btn_text; ?></button>
		                    <input type="hidden" name="submitted" id="submitted" value="true" />
		               </div>
		        </form>
            </div>
            <div class="col-md-3">
            </div>
         <?php } ?>
    </div>
</div>