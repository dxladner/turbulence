<?php
if(isset($_POST['submitted'])) 
{
    if(trim($_POST['email']) === '')  
    {
        $emailError = true;
        $hasError = true;
    } 
    else if (!preg_match("/^[[:alnum:]][a-z0-9_.-]*@[a-z0-9.-]+\.[a-z]{2,4}$/i", trim($_POST['email']))) 
    {
        $emailError = true;
        $hasError = true;
    } 
    else 
    {
        $email = trim($_POST['email']);
    }

    if(!isset($hasError)) 
    {
        $emailTo = get_option('admin_email');
        if (!isset($emailTo) || ($emailTo == '') )
        {
            $emailTo = get_option('admin_email');
        }
        $name = 'Simple Subscription Form';
        $subject = __('From ','turbulence').$name;
        $body = __('Name: ','turbulence').$name."\n".__('Email: ','turbulence').$email."\n".__('Comments: ','turbulence');
        $headers = __('From: ','turbulence') .$name. ' <'.$emailTo.'>' . "\r\n" . __('Reply-To:','turbulence') .$name. '<'.$email.'>';

        wp_mail($emailTo, $subject, $body, $headers);
        $emailSent = true;
    }

}
	$lp_btn_text = get_theme_mod('landing_page_button_text', '');
	$lp_btn_size = 'btn-'.get_theme_mod('landing_page_button_size', '');
	$lp_btn_color = 'btn-'.get_theme_mod('landing_page_button_color', '');
?>
<div class="simpleSubscribe">
    <div class="row">
        <?php if(isset($emailSent) && $emailSent == true) { ?>
            <div class="alert alert-success" role="alert">
                <p><?php _e('Thanks, you have submitted the form successfully!', 'turbulence'); ?></p>
            </div>
        <?php } else { ?>

            <?php if(isset($hasError) || isset($captchaError)) { ?>
                <div class="alert alert-danger alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				  <strong><?php _e('Error!', 'turbulence'); ?></strong> <?php _e('Please try again.', 'turbulence'); ?>
				</div>
            <?php } ?>
            <div class="col-md-3">
            </div>
            <div class="col-md-6">
                <form action="<?php the_permalink(); ?>" id="simpleSubscribeForm" method="post" class="form-inline centeredText" role="form">
                   <div class="form-group <?php if(isset($emailError)) { echo "has-error has-feedback"; }?>">
                        <input class="form-control input-lg" type="text" name="email" id="email" value="" placeholder="Email Address" />
                        <?php if(isset($emailError)) { ?>
                            <span class="glyphicon glyphicon-remove form-control-feedback"></span>
                        <?php } ?>
		                <button type="submit" class="btn <?php echo $lp_btn_color .' '. $lp_btn_size; ?>"><?php echo $lp_btn_text; ?></button>
                        <input type="hidden" name="submitted" id="submitted" value="true" />
                   </div>
                </form>
            </div>
            <div class="col-md-3">
            </div>
         <?php } ?>
    </div>
</div>