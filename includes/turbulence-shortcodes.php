<?php 

if (!function_exists('create_hyperdrive_button')) {
	function create_hyperdrive_button($atts) {
    $atts = shortcode_atts(
        array(
       		'color' 		=> 'blue',
			'size' 			=> 'medium',
			'style' 		=> 'square',
			'fontweight'	=> 'normal',
			'text'			=> 'Submit',
			'url' 			=> '#'
    ), $atts);
    return '
	<a class="hyperdrive-button '.$atts['color'].' '.$atts['size'].' '. $atts['style'] .' '.$atts['fontweight'].' "   href="http://'.$atts['url'].'">' . $atts['text'] . '</a>';
   }
	add_shortcode('hyperdrive-button', 'create_hyperdrive_button');
}

if (!function_exists('create_hyperdrive_box')) {
	function create_hyperdrive_box($atts) {
    $atts = shortcode_atts(
        array(
			'title'			=> 'Title',
			'color'			=> '',
			'boxtext' 		=> '',  
    ), $atts);
	return '<div class="hyperdrive-box '.$atts['color'].' ">
				<div class="hyperdrive-box-title '.$atts['color'].' ">' . $atts['title'] . 
				'</div>
				<div class="hyperdrive-box-content '.$atts['color'].'">' . $atts['boxtext'] . '</div>
			</div>';

   }
	add_shortcode('hyperdrive-box', 'create_hyperdrive_box');
}

if (!function_exists('create_hyperdrive_dropcap')) {
	function create_hyperdrive_dropcap($atts) {
    $atts = shortcode_atts(
        array(
			'style'				=> 'simple',
			'color'				=> '',
			'dropcapletter' 	=> '',
			'dropcaptext' 		=> ''  
    ), $atts);
	return '<div class="hyperdrive-dropcap '.$atts['style'].' '.$atts['color'].' "><span class="firstcharacter '.$atts['color'].'">' . $atts['dropcapletter'] . '
			</span></div><p>' . $atts['dropcaptext'] . '</p>';
   }
	add_shortcode('hyperdrive-dropcap', 'create_hyperdrive_dropcap');
}

if (!function_exists('create_hyperdrive_highlighter')) {
	function create_hyperdrive_highlighter($atts) {
    $atts = shortcode_atts(
        array(
			'color'				=> '',
			'highlightertext' 	=> '' 
    ), $atts);
	return '<span class="hyperdrive-highlighter '.$atts['color'].'">' . $atts['highlightertext'] . '
			</span>';
   }
	add_shortcode('hyperdrive-highlighter', 'create_hyperdrive_highlighter');
}

if (!function_exists('create_hyperdrive_blockquote')) {
	function create_hyperdrive_blockquote($atts) {
    $atts = shortcode_atts(
        array(
			'color'				=> '',
			'align'				=> '',
			'author'			=> '',
			'blockquotetext' 	=> '' 
    ), $atts);
	return '<blockquote class="hyperdrive-blockquote '.$atts['color'].'">' . $atts['blockquotetext'] . '
			<p><cite> ~' . $atts['author'] . '</cite></p>
			</blockquote>';
   }
	add_shortcode('hyperdrive-blockquote', 'create_hyperdrive_blockquote');
}

if (!function_exists('create_hyperdrive_color_box')) {
	function create_hyperdrive_color_box($atts) {
    $atts = shortcode_atts(
        array(
			'color'				=> '',
			'fontsize'			=> '',
			'colorboxtext' 		=> '' 
    ), $atts);
	return '<div class="hyperdrive-color-box '.$atts['color'].' '.$atts['fontsize'].' ">
			<p>' . $atts['colorboxtext'] . '</p>
			</div>';
   }
	add_shortcode('hyperdrive-color-box', 'create_hyperdrive_color_box');
}

if (!function_exists('create_hyperdrive_alert')) {
	function create_hyperdrive_alert($atts) {
    $atts = shortcode_atts(
        array(
			'color'				=> '',
			'type'				=> '',
			'alerttext' 		=> '' 
    ), $atts);
		return '<div class="hyperdrive-alert '.$atts['color'].' ">
			<p><i class=" '.$atts['type'].' "></i>' . $atts['alerttext'] . '</p>
			</div>';
   }
	add_shortcode('hyperdrive-alert', 'create_hyperdrive_alert');
}
if (!function_exists('create_hyperdrive_spoiler')) {
    function create_hyperdrive_spoiler( $atts, $content = null ) {
		extract( shortcode_atts( array(
				'color'				=> '',
			'size'				=> '',
			'spoilertitle'		=> '',
			'spoilertext' 		=> ''
				), $atts ) );

		return '<div class="hyperdrive-spoiler hyperdrive-spoiler-open ">
		            <div class="hyperdrive-spoiler-title ' . $color . ' ' . $size . ' ">&nbsp;&nbsp;' . $spoilertitle . '
		            </div>
		            <div class="hyperdrive-spoiler-content ' . $color . ' ' . $size . ' ">' . do_shortcode( $spoilertext ) . '
		            </div>
		        </div>';
	}
	add_shortcode('hyperdrive-spoiler', 'create_hyperdrive_spoiler');
}
if (!function_exists('create_hyperdrive_list')) {
    function create_hyperdrive_list($atts, $content = null) {  

    extract(shortcode_atts(array(
                'icon' => '',
                'color' => '',
                'listtext' => ''
        ), $atts));     	

        return '<ul class="hyperdrive-list '.$icon. ' '.$color.' ">
                ' . do_shortcode($listtext) . '
                </ul>';
	}	
	add_shortcode('hyperdrive-list', 'create_hyperdrive_list');
}

if (!function_exists('create_hyperdrive_tabs')) {
function create_hyperdrive_tabs($atts, $content = null) {  

    extract(shortcode_atts(array(
             	'tabone' 			=> '',
				'tabtwo' 			=> '',
				'tabthree'			=> '',
				'tabfour' 			=> '',
				'tabonebodytext'	=> '',
				'tabtwobodytext'	=> '',
				'tabthreeebodytext'	=> '',
				'tabfourbodytext'	=> ''
				), $atts));       	

		  return 
		'<div role="tabpanel">
		  <ul class="nav nav-tabs" role="tablist">
			<li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">' . $tabone . '</a></li>
			<li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">' . $tabtwo . '</a></li>
			<li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">' . $tabthree . '</a></li>
			<li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">' . $tabfour . '</a></li>
		  </ul>
		  <div class="tab-content">
			<div role="tabpanel" class="tab-pane active" id="home">' . $tabonebodytext . '</div>
			<div role="tabpanel" class="tab-pane" id="profile">' . $tabtwobodytext . '</div>
			<div role="tabpanel" class="tab-pane" id="messages">' . $tabthreeebodytext . '</div>
			<div role="tabpanel" class="tab-pane" id="settings">' . $tabfourbodytext . '</div>
		  </div>
		</div>';
    }
	add_shortcode('hyperdrive-tabs', 'create_hyperdrive_tabs');
}

if (!function_exists('create_hyperdrive_line')) {
    function create_hyperdrive_line($atts, $content = null) {  

    extract(shortcode_atts(array(
                'color' => '',
                'width' => '',
                'height' => ''
        ), $atts));     	

        return '<hr class="hyperdrive-line ' . $color . ' ' . $width . ' ' . $height . ' ">';
	}	
	add_shortcode('hyperdrive-line', 'create_hyperdrive_line');
}

if (!function_exists('create_hyperdrive_modal')) {
    function create_hyperdrive_modal($atts, $content = null) {  

    extract(shortcode_atts(array(
				'bootstrapbuttontype' => '',
				'bootstrapbuttonsize' => '',
                'buttontext' => '',
                'modalbodytext' => ''
               
        ), $atts));     	

        return 
		'<button type="button" class="' . $bootstrapbuttontype . ' ' . $bootstrapbuttonsize . '" data-toggle="modal" data-target="#myModal">
		  ' . $buttontext . '
		</button>
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		  <div class="modal-dialog">
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title" id="myModalLabel">Modal title</h4>
			  </div>
			  <div class="modal-body">
				' . $modalbodytext . '
			  </div>
			  <div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			  </div>
			</div>
		  </div>
		</div>';
	}	
	add_shortcode('hyperdrive-modal', 'create_hyperdrive_modal');
}

if (!function_exists('create_hyperdrive_tooltip')) {
function create_hyperdrive_tooltip($atts, $content = null) {  

    extract(shortcode_atts(array(
				'tooltipcolor' 							=> '',
				'tooltiptextbackgroundcolor' 			=> '',
             	'tooltipplacement' 						=> '',
				'tooltipabovetext' 							=> '',
				'tooltipinlinetext' 							=> ''
				), $atts));       	

		  return 
		'<span class="hyperdrive-tooltip ' . $tooltipcolor . ' ' . $tooltiptextbackgroundcolor . '" data-toggle="tooltip" data-placement="' . $tooltipplacement . '" title="' . $tooltipabovetext . '">' . $tooltipinlinetext . '</span>';
    }
	add_shortcode('hyperdrive-tooltip', 'create_hyperdrive_tooltip');
}

if (!function_exists('create_hyperdrive_accordion')) {
function create_hyperdrive_accordion($atts, $content = null) {  

    extract(shortcode_atts(array(
             	'titleone' 			=> '',
				'bodyone' 			=> '',
				'titletwo' 			=> '',
				'bodytwo' 			=> '',
				'titlethree' 		=> '',
				'bodythree' 		=> '',
				'titlefour' 		=> '',
				'bodyfour' 			=> ''
				), $atts));       	

		  return 
		'<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
		  	<div class="panel panel-default">
				<div class="panel-heading" role="tab" id="headingOne">
			 		<h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">'.$titleone.'</a>
			  		</h4>
				</div>
				<div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
			  		<div class="panel-body">
						'.$bodyone.'
			  		</div>
				</div>
		  	</div>
			<div class="panel panel-default">
				<div class="panel-heading" role="tab" id="headingTwo">
			  		<h4 class="panel-title">
						<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
				  		'.$titletwo.'
						</a>
			  		</h4>
				</div>
				<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
			  		<div class="panel-body">
						'.$bodytwo.'
			  		</div>
				</div>
		  	</div>
			<div class="panel panel-default">
				<div class="panel-heading" role="tab" id="headingThree">
			  		<h4 class="panel-title">
						<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
				  		'.$titlethree.'
						</a>
			  		</h4>
				</div>
				<div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
			  		<div class="panel-body">
						'.$bodythree.'
			  		</div>
				</div>
		  	</div>
			<div class="panel panel-default">
				<div class="panel-heading" role="tab" id="headingFour">
			  		<h4 class="panel-title">
						<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="true" aria-controls="collapseFour">
				  		'.$titlefour.' 
						</a>
			  		</h4>
				</div>
				<div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
			  		<div class="panel-body">
						'.$bodyfour.' 
			  		</div>
				</div>
		  	</div>
		</div>
		</div>';
    }
	add_shortcode('hyperdrive-accordion', 'create_hyperdrive_accordion');
}

if (!function_exists('create_hyperdrive_carousel')) {
function create_hyperdrive_carousel($atts, $content = null) {  

    extract(shortcode_atts(array(
             	'imageone' 					=> '',
				'imageonecaption' 			=> '',
				'imagetwo' 					=> '',
				'imagetwocaption' 			=> '',
				'imagethree' 				=> '',
				'imagethreecaption' 		=> ''
				), $atts));         	

		  return 
		'<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
			  <!-- Indicators -->
			  <ol class="carousel-indicators">
				<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
				<li data-target="#carousel-example-generic" data-slide-to="1"></li>
				<li data-target="#carousel-example-generic" data-slide-to="2"></li>
			  </ol>

			  <!-- Wrapper for slides -->
			  <div class="carousel-inner" role="listbox">
				<div class="item active">
				  <img src="'. $imageone .'">
				  <div class="carousel-caption">
				  '. $imageonecaption .'
				  </div>
				</div>
				<div class="item">
				  <img src="'. $imagetwo .'">
				  <div class="carousel-caption">
					'. $imagetwocaption .'
				  </div>
				</div>
				<div class="item">
				   <img src="'. $imagethree .'">
				  <div class="carousel-caption">
					'. $imagethreecaption .'
				  </div>
				</div>
			  </div>

			  <!-- Controls -->
			  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
				<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
				<span class="sr-only">Previous</span>
			  </a>
			  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
				<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
				<span class="sr-only">Next</span>
			  </a>
			</div>';
    }
	add_shortcode('hyperdrive-carousel', 'create_hyperdrive_carousel');
}