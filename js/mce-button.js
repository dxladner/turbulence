(function() {
	tinymce.PluginManager.add('my_mce_button', function( editor, url ) {
		editor.addButton( 'my_mce_button', {
			text: 'Turbulence Shortcodes',
			icon: false,
			type: 'menubutton',
			menu: [
				{
					text: 'Button',
					menu: [
						{
							text: 'Button Options',
							onclick: function() {
								editor.windowManager.open( {
									title: 'Insert Button Options',
									body: [
										{
											type: 'listbox',
											name: 'color',
											label: 'Color',
											'values': [
												{text: 'Blue', value: 'blue'},
												{text: 'Red', value: 'red'},
												{text: 'Green', value: 'green'},
												{text: 'Black', value: 'black'},
												{text: 'Grey', value: 'grey'},
												{text: 'Purple', value: 'purple'},
												{text: 'Orange', value: 'orange'},
												{text: 'Light-Blue', value: 'lightBlue'},
												{text: 'Yellow', value: 'yellow'},
												{text: 'White', value: 'white'}
											]
										},
										{
											type: 'listbox',
											name: 'size',
											label: 'Size',
											'values': [
												{text: 'X-Small', value: 'xsmall'},
												{text: 'Small', value: 'small'},
												{text: 'Medium', value: 'medium'},
												{text: 'Large', value: 'large'},
												{text: 'X-Large', value: 'xlarge'},
												{text: 'Super Size', value: 'supersize'}
											]
										},
										{
											type: 'listbox',
											name: 'style',
											label: 'Style',
											'values': [
												{text: 'Round', value: 'round'},
												{text: 'Square', value: 'square'},
												{text: 'Less Round', value: 'lessround'}
											]
										},
										{
											type: 'listbox',
											name: 'fontweight',
											label: 'Font Weight',
											'values': [
												{text: 'Normal', value: 'normal'},
												{text: 'Bold', value: 'bold'},
												{text: 'Bolder', value: 'bolder'}
											]
										},
										{
											type: 'textbox',
											name: 'text',
											label: 'Button Text',
											value: ''
										},
										{
											type: 'textbox',
											name: 'url',
											label: 'URL',
											value: ''
										}
										
									],
									onsubmit: function( e ) {
										editor.insertContent( '[hyperdrive-button color="' + e.data.color + '" size="' + e.data.size + '" style="' + e.data.style + '" fontweight="' + e.data.fontweight + '" text="' + e.data.text + '" url="' + e.data.url + '"]');
									}
								});
							}
						}
					]
					
				},
				
				{
					text: 'Info Content Boxes',
					menu: [
						{
							text: 'Info Content Boxes Options',
							onclick: function() {
								editor.windowManager.open( {
									title: 'Info Content Boxes Options',
									body: [
										{
											type: 'textbox',
											name: 'title',
											label: 'Title',
											value: ''
										},
										{
											type: 'listbox',
											name: 'color',
											label: 'Color',
											'values': [
												{text: 'Blue', value: 'blue'},
												{text: 'Red', value: 'red'},
												{text: 'Green', value: 'green'},
												{text: 'Black', value: 'black'},
												{text: 'Grey', value: 'grey'},
												{text: 'Purple', value: 'purple'},
												{text: 'Orange', value: 'orange'},
												{text: 'Light-Blue', value: 'lightBlue'},
												{text: 'Yellow', value: 'yellow'},
												{text: 'White', value: 'white'}
											]
										},
										{
											type: 'textbox',
											name: 'boxtext',
											label: 'Box Text',
											value: '',
											multiline: true,
											minWidth: 300,
											minHeight: 100
										}	
									],
									onsubmit: function( e ) {
										editor.insertContent( '[hyperdrive-box title="' + e.data.title + '" color="' + e.data.color + '" boxtext="' + e.data.boxtext + '"]');
									}
								});
							}
						}
					]
					
				},
				
				{
					text: 'DropCap',
					menu: [
						{
							text: 'Info Content Boxes Options',
							onclick: function() {
								editor.windowManager.open( {
									title: 'Info Content Boxes Options',
									body: [
										{
											type: 'listbox',
											name: 'style',
											label: 'Style',
											'values': [
												{text: 'Simple', value: 'simple'},
												{text: 'Square', value: 'square'},
												{text: 'Circle', value: 'circle'}
											]
										},
										{
											type: 'listbox',
											name: 'color',
											label: 'Color',
											'values': [
												{text: 'Blue', value: 'blue'},
												{text: 'Red', value: 'red'},
												{text: 'Green', value: 'green'},
												{text: 'Black', value: 'black'},
												{text: 'Grey', value: 'grey'},
												{text: 'Purple', value: 'purple'},
												{text: 'Orange', value: 'orange'},
												{text: 'Light-Blue', value: 'lightBlue'},
												{text: 'Yellow', value: 'yellow'},
												{text: 'White', value: 'white'}
											]
										},
										{
											type: 'textbox',
											name: 'dropcapletter',
											label: 'DropCap Letter',
											value: ''
										},
										{
											type: 'textbox',
											name: 'dropcaptext',
											label: 'DropCap Text',
											value: '',
											multiline: true,
											minWidth: 300,
											minHeight: 100
										}	
									],
									onsubmit: function( e ) {
										editor.insertContent( '[hyperdrive-dropcap style="' + e.data.style + '" color="' + e.data.color + '" dropcapletter="' + e.data.dropcapletter + '" dropcaptext="' + e.data.dropcaptext +'"]');
									}
								});
							}
						}
					]
					
				},
				
				{
					text: 'Highlighter',
					menu: [
						{
							text: 'Highlighter Options',
							onclick: function() {
								editor.windowManager.open( {
									title: 'Highlighter Options',
									body: [
										{
											type: 'listbox',
											name: 'color',
											label: 'Color',
											'values': [
												{text: 'Blue', value: 'blue'},
												{text: 'Red', value: 'red'},
												{text: 'Green', value: 'green'},
												{text: 'Black', value: 'black'},
												{text: 'Grey', value: 'grey'},
												{text: 'Purple', value: 'purple'},
												{text: 'Orange', value: 'orange'},
												{text: 'Light-Blue', value: 'lightBlue'},
												{text: 'Yellow', value: 'yellow'},
												{text: 'White', value: 'white'}
											]
										},
										{
											type: 'textbox',
											name: 'highlightertext',
											label: 'Highlighter Content',
											value: ''
										}	
									],
									onsubmit: function( e ) {
										editor.insertContent( '[hyperdrive-highlighter color="' + e.data.color + '" highlightertext="' + e.data.highlightertext +'"]');
									}
								});
							}
						}
					]
					
				},
				
				{
					text: 'Block Quotes',
					menu: [
						{
							text: 'Block Quotes Options',
							onclick: function() {
								editor.windowManager.open( {
									title: 'Block Quotes Options',
									body: [
										{
											type: 'listbox',
											name: 'color',
											label: 'Color',
											'values': [
												{text: 'Blue', value: 'blue'},
												{text: 'Red', value: 'red'},
												{text: 'Green', value: 'green'},
												{text: 'Black', value: 'black'},
												{text: 'Grey', value: 'grey'},
												{text: 'Purple', value: 'purple'},
												{text: 'Orange', value: 'orange'},
												{text: 'Light-Blue', value: 'lightBlue'},
												{text: 'Yellow', value: 'yellow'},
												{text: 'White', value: 'white'}
											]
										},
										{
											type: 'listbox',
											name: 'align',
											label: 'Align',
											'values': [
												{text: 'Left', value: 'left'},
												{text: 'Right', value: 'right'},
												{text: 'None', value: 'none'}
											]
										},
										{
											type: 'textbox',
											name: 'author',
											label: 'Author',
											value: ''
										},
										{
											type: 'textbox',
											name: 'blockquotetext',
											label: 'Blockquote Content',
											value: '',
											multiline: true,
											minWidth: 300,
											minHeight: 100
										}	
									],
									onsubmit: function( e ) {
										editor.insertContent( '[hyperdrive-blockquote color="' + e.data.color + '"  align="' + e.data.align +'" author="' + e.data.author + '" blockquotetext="' + e.data.blockquotetext + '"]');
									}
								});
							}
						}
					]
					
				},
				
				{
					text: 'Color Boxes',
					menu: [
						{
							text: 'Color Boxes Options',
							onclick: function() {
								editor.windowManager.open( {
									title: 'Color Boxes Options',
									body: [
										{
											type: 'listbox',
											name: 'color',
											label: 'Color',
											'values': [
												{text: 'Blue', value: 'blue'},
												{text: 'Red', value: 'red'},
												{text: 'Green', value: 'green'},
												{text: 'Black', value: 'black'},
												{text: 'Grey', value: 'grey'},
												{text: 'Purple', value: 'purple'},
												{text: 'Orange', value: 'orange'},
												{text: 'Light-Blue', value: 'lightBlue'},
												{text: 'Yellow', value: 'yellow'},
												{text: 'White', value: 'white'}
											]
										},
										{
											type: 'listbox',
											name: 'fontsize',
											label: 'Font Size',
											'values': [
												{text: 'X Small', value: 'xsmall'},
												{text: 'Small', value: 'small'},
												{text: 'Medium', value: 'medium'},
												{text: 'Large', value: 'large'},
												{text: 'XLarge', value: 'xlarge'}
											]
										},
										{
											type: 'textbox',
											name: 'colorboxtext',
											label: 'Color Box Content',
											value: ''
										}
									],
									onsubmit: function( e ) {
										editor.insertContent( '[hyperdrive-color-box color="' + e.data.color + '"  fontsize="' + e.data.fontsize +'" colorboxtext="' + e.data.colorboxtext + '"]');
									}
								});
							}
						}
					]
					
				},
				
				{
					text: 'Alert',
					menu: [
						{
							text: 'Alert Options',
							onclick: function() {
								editor.windowManager.open( {
									title: 'Alert Options',
									body: [
										{
											type: 'listbox',
											name: 'type',
											label: 'Type',
											'values': [
												{text: 'Buy Now', value: 'fa fa-money fa-4x'},
												{text: 'Call', value: 'fa fa-phone fa-4x'},
												{text: 'Download', value: 'fa fa-cloud-download fa-4x'},
												{text: 'Email', value: 'fa fa-envelope-o fa-4x'},
												{text: 'Error', value: 'fa fa-times fa-4x'},
												{text: 'Hint', value: 'fa fa-bullseye fa-4x'},
												{text: 'Info', value: 'fa fa-info fa-4x'},
												{text: 'Message', value: 'fa fa-envelope fa-4x'},
												{text: 'Print', value: 'fa fa-print fa-4x'},
												{text: 'Secure', value: 'fa fa-lock fa-4x'},
												{text: 'Success', value: 'fa fa-check fa-4x'},
												{text: 'User', value: 'fa fa-user fa-4x'},
												{text: 'Users', value: 'fa fa-users fa-4x'},
												{text: 'Video', value: 'fa fa-video-camera fa-4x'},
												{text: 'Warning', value: 'fa fa-exclamation-triangle fa-4x'},
												{text: 'WiFi', value: 'fa fa-wifi fa-4x'}		
											]
										},
										{
											type: 'textbox',
											name: 'alerttext',
											label: 'Alert Content',
											value: ''
										}
									],
									onsubmit: function( e ) {
										editor.insertContent( '[hyperdrive-alert type="' + e.data.type + '" alerttext="' + e.data.alerttext + '"]');
									}
								});
							}
						}
					]
					
				},
				
				{
					text: 'List',
					menu: [
						{
							text: 'List Options',
							onclick: function() {
								editor.windowManager.open( {
									title: 'List Options',
									body: [
										{
											type: 'listbox',
											name: 'color',
											label: 'Color',
											'values': [
												{text: 'Blue', value: 'blue'},
												{text: 'Red', value: 'red'},
												{text: 'Green', value: 'green'},
												{text: 'Black', value: 'black'},
												{text: 'Grey', value: 'grey'},
												{text: 'Purple', value: 'purple'},
												{text: 'Orange', value: 'orange'},
												{text: 'Light-Blue', value: 'lightBlue'},
												{text: 'Yellow', value: 'yellow'},
												{text: 'White', value: 'white'}
											]
										},
										{
											type: 'listbox',
											name: 'icon',
											label: 'Icon',
											'values': [
												{text: 'Buy Now', value: 'fa fa-money fa-2x'},
												{text: 'Call', value: 'fa fa-phone fa-2x'},
												{text: 'Download', value: 'fa fa-cloud-download fa-2x'},
												{text: 'Email', value: 'fa fa-envelope-o fa-2x'},
												{text: 'Error', value: 'fa fa-times fa-2x'},
												{text: 'Hint', value: 'fa fa-bullseye fa-2x'},
												{text: 'Info', value: 'fa fa-info fa-2x'},
												{text: 'Message', value: 'fa fa-envelope fa-2x'},
												{text: 'Print', value: 'fa fa-print fa-2x'},
												{text: 'Secure', value: 'fa fa-lock fa-2x'},
												{text: 'Success', value: 'fa fa-check fa-2x'},
												{text: 'User', value: 'fa fa-user fa-2x'},
												{text: 'Users', value: 'fa fa-users fa-2x'},
												{text: 'Video', value: 'fa fa-video-camera fa-2x'},
												{text: 'Warning', value: 'fa fa-exclamation-triangle fa-2x'},
												{text: 'WiFi', value: 'fa fa-wifi fa-2x'}		
											]
										},
										{
											type: 'textbox',
											name: 'listtext',
											label: 'List Content',
											value: 'List Content: To create your list, place text in textarea like this: <li>Content 1</li><li>Content2</li>',
											multiline: true,
											minWidth: 300,
											minHeight: 100
										}
									],
									onsubmit: function( e ) {
										editor.insertContent( '[hyperdrive-list color="' + e.data.color + '" icon="' + e.data.icon + '" listtext="' + e.data.listtext + '"]');
									}
								});
							}
						}
					]
					
				},
				
				{
					text: 'Spoiler',
					menu: [
						{
							text: 'Spoiler Options',
							onclick: function() {
								editor.windowManager.open( {
									title: 'Spoiler Options',
									body: [
										{
											type: 'listbox',
											name: 'color',
											label: 'Color',
											'values': [
												{text: 'Blue', value: 'blue'},
												{text: 'Red', value: 'red'},
												{text: 'Green', value: 'green'},
												{text: 'Black', value: 'black'},
												{text: 'Grey', value: 'grey'},
												{text: 'Purple', value: 'purple'},
												{text: 'Orange', value: 'orange'},
												{text: 'Light-Blue', value: 'lightBlue'},
												{text: 'Yellow', value: 'yellow'},
												{text: 'White', value: 'white'}
											]
										},
										{
											type: 'listbox',
											name: 'size',
											label: 'Size',
											'values': [
												{text: 'X-Small', value: 'xsmall'},
												{text: 'Small', value: 'small'},
												{text: 'Medium', value: 'medium'},
												{text: 'Large', value: 'large'},
												{text: 'X-Large', value: 'xlarge'},
												{text: 'Super Size', value: 'supersize'}
											]
										},
										{
											type: 'textbox',
											name: 'spoilertitle',
											label: 'Spoiler Title',
											value: ''
										},
										{
											type: 'textbox',
											name: 'spoilertext',
											label: 'Spoiler Text',
											value: '',
											multiline: true,
											minWidth: 300,
											minHeight: 100
										}
									],
									onsubmit: function( e ) {
										editor.insertContent( '[hyperdrive-spoiler color="' + e.data.color + '" size="' + e.data.size + '" spoilertitle="' + e.data.spoilertitle + '" spoilertext="' + e.data.spoilertext + '"]');
									}
								});
							}
						}
					]
					
				},
				
				{
					text: 'Horizontal Lines',
					menu: [
						{
							text: 'Horizontal Lines Options',
							onclick: function() {
								editor.windowManager.open( {
									title: 'Horizontal Lines Options',
									body: [
										{
											type: 'listbox',
											name: 'color',
											label: 'Color',
											'values': [
												{text: 'Blue', value: 'blue'},
												{text: 'Red', value: 'red'},
												{text: 'Green', value: 'green'},
												{text: 'Black', value: 'black'},
												{text: 'Grey', value: 'grey'},
												{text: 'Purple', value: 'purple'},
												{text: 'Orange', value: 'orange'},
												{text: 'Light-Blue', value: 'lightBlue'},
												{text: 'Yellow', value: 'yellow'},
												{text: 'White', value: 'white'}
											]
										},
										{
											type: 'listbox',
											name: 'width',
											label: 'Width',
											'values': [
												{text: '25%', value: 'quarterwidth'},
												{text: '50%', value: 'halfwidth'},
												{text: '75%', value: 'threequarterwidth'},
												{text: '90%', value: 'ninetywidth'},
												{text: '100%', value: 'fullwidth'}
											]
										},
										{
											type: 'listbox',
											name: 'height',
											label: 'Height',
											'values': [
												{text: 'X-Small', value: 'xsmall'},
												{text: 'Small', value: 'small'},
												{text: 'Medium', value: 'medium'},
												{text: 'Large', value: 'large'},
												{text: 'X-Large', value: 'xlarge'},
												{text: 'Super Size', value: 'supersize'}
											]
										}
									],
									onsubmit: function( e ) {
										editor.insertContent( '[hyperdrive-line color="' + e.data.color + '"  width="' + e.data.width +'" height="' + e.data.height + '"]');
									}
								});
							}
						}
					]
					
				},
				
				{
					text: 'Modal',
					menu: [
						{
							text: 'Modal Options',
							onclick: function() {
								editor.windowManager.open( {
									title: 'Modal Options',
									body: [
										{
											type: 'listbox',
											name: 'bootstrapbuttontype',
											label: 'Buttonstrap Button Type',
											'values': [
												{text: 'Default', value: 'btn btn-default'},
												{text: 'Primary', value: 'btn btn-primary'},
												{text: 'Success', value: 'btn btn-success'},
												{text: 'Info', value: 'btn btn-info'},
												{text: 'Warning', value: 'btn btn-warning'},
												{text: 'Danger', value: 'btn btn-danger'}
											]
										},
										{
											type: 'listbox',
											name: 'bootstrapbuttonsize',
											label: 'Buttonstrap Button Size',
											'values': [
												{text: 'X Small', value: 'btn-xs'},
												{text: 'Small', value: 'btn-sm'},
												{text: 'Default', value: ''},
												{text: 'Large', value: 'btn-lg'}
											]
										},
										{
											type: 'textbox',
											name: 'buttontext',
											label: 'Modal Button Text',
											value: ''
										},
										{
											type: 'textbox',
											name: 'modalbodytext',
											label: 'Modal Body Text',
											value: '',
											multiline: true,
											minWidth: 300,
											minHeight: 100
										}
									],
									onsubmit: function( e ) {
										editor.insertContent( '[hyperdrive-modal bootstrapbuttontype="' + e.data.bootstrapbuttontype + '" bootstrapbuttonsize="' + e.data.bootstrapbuttonsize + '" buttontext="' + e.data.buttontext + '" modalbodytext="' + e.data.modalbodytext + '"]');
									}
								});
							}
						}
					]
					
				},
				
				{
					text: 'Tooltip',
					menu: [
						{
							text: 'Tooltip Options',
							onclick: function() {
								editor.windowManager.open( {
									title: 'Tooltip Options',
									body: [
										{
											type: 'listbox',
											name: 'tooltipcolor',
											label: 'Tooltip Color',
											'values': [
												{text: 'Blue', value: 'blue'},
												{text: 'Red', value: 'red'},
												{text: 'Green', value: 'green'},
												{text: 'Black', value: 'black'},
												{text: 'Grey', value: 'grey'},
												{text: 'Purple', value: 'purple'},
												{text: 'Orange', value: 'orange'},
												{text: 'Light-Blue', value: 'lightBlue'},
												{text: 'Yellow', value: 'yellow'},
												{text: 'White', value: 'white'}
											]
										},
										{
											type: 'listbox',
											name: 'tooltiptextbackgroundcolor',
											label: 'Tooltip Text Background Color',
											'values': [
												{text: 'Blue', value: 'blue-tooltip'},
												{text: 'Red', value: 'red-tooltip'},
												{text: 'Green', value: 'green-tooltip'},
												{text: 'Black', value: 'black-tooltip'},
												{text: 'Grey', value: 'grey-tooltip'},
												{text: 'Purple', value: 'purple-tooltip'},
												{text: 'Orange', value: 'orange-tooltip'},
												{text: 'Light-Blue', value: 'lightBlue-tooltip'},
												{text: 'Yellow', value: 'yellow-tooltip'},
												{text: 'White', value: 'white-tooltip'}
											]
										},
										{
											type: 'listbox',
											name: 'tooltipplacement',
											label: 'Tooltip Placement',
											'values': [
												{text: 'Left', value: 'left'},
												{text: 'Right', value: 'right'},
												{text: 'Top', value: 'top'},
												{text: 'Bottom', value: 'bottom'}
											]
										},
										
										{
											type: 'textbox',
											name: 'tooltipabovetext',
											label: 'Tooltip Above Text',
											value: ''
										},
										{
											type: 'textbox',
											name: 'tooltipinlinetext',
											label: 'Tooltip Inline Text',
											value: ''
										}
									],
									onsubmit: function( e ) {
										editor.insertContent( '[hyperdrive-tooltip tooltipcolor="' + e.data.tooltipcolor + '" tooltiptextbackgroundcolor="' + e.data.tooltiptextbackgroundcolor + '" tooltipplacement="' + e.data.tooltipplacement + '" tooltipabovetext="' + e.data.tooltipabovetext + '" tooltipinlinetext="' + e.data.tooltipinlinetext + '"]');
									}
								});
							}
						}
					]
					
				},
				{
					text: 'Carousel',
					menu: [
						{
							text: 'Carousel Options',
							onclick: function() {
								editor.windowManager.open( {
									title: 'Carousel Options',
									body: [
										{
											type: 'textbox',
											name: 'imageone',
											label: 'Image One URL',
											value: ''
										},
										{
											type: 'textbox',
											name: 'imageonecaption',
											label: 'Image One Caption',
											value: ''
										},
										{
											type: 'textbox',
											name: 'imagetwo',
											label: 'Image Two URL',
											value: ''
										},
										{
											type: 'textbox',
											name: 'imagetwocaption',
											label: 'Image Two Caption',
											value: ''
										},
										{
											type: 'textbox',
											name: 'imagethree',
											label: 'Image Three URL',
											value: ''
										},
										{
											type: 'textbox',
											name: 'imagethreecaption',
											label: 'Image Three Caption',
											value: ''
										}
										
									],
									onsubmit: function( e ) {
										editor.insertContent( '[hyperdrive-carousel imageone="' + e.data.imageone + '"  imageonecaption="' + e.data.imageonecaption +'" imagetwo="' + e.data.imagetwo + '" imagetwocaption="' + e.data.imagetwocaption + '" imagethree="' + e.data.imagethree + '" imagethreecaption="' + e.data.imagethreecaption + '"]');
									}
								});
							}
						}
					]
					
				},
				{
					text: 'Accordion',
					menu: [
						{
							text: 'Accordion Options',
							onclick: function() {
								editor.windowManager.open( {
									title: 'Accordion Options',
									body: [
										{
											type: 'textbox',
											name: 'titleone',
											label: 'Accordion Title One',
											value: ''
										},
										{
											type: 'textbox',
											name: 'bodyone',
											label: 'Accordion Body One',
											value: '',
											multiline: true,
											minWidth: 300,
											minHeight: 100
										},
										{
											type: 'textbox',
											name: 'titletwo',
											label: 'Accordion Title Two',
											value: ''
										},
										{
											type: 'textbox',
											name: 'bodytwo',
											label: 'Accordion Body Two',
											value: '',
											multiline: true,
											minWidth: 300,
											minHeight: 100
										},
										{
											type: 'textbox',
											name: 'titlethree',
											label: 'Accordion Title Three',
											value: ''
										},
										{
											type: 'textbox',
											name: 'bodythree',
											label: 'Accordion Body Three',
											value: '',
											multiline: true,
											minWidth: 300,
											minHeight: 100
										},
										{
											type: 'textbox',
											name: 'titlefour',
											label: 'Accordion Title Four',
											value: ''
										},
										{
											type: 'textbox',
											name: 'bodyfour',
											label: 'Accordion Body Four',
											value: '',
											multiline: true,
											minWidth: 300,
											minHeight: 100
										}
										
									],
									onsubmit: function( e ) {
										editor.insertContent( '[hyperdrive-accordion titleone="' + e.data.titleone + '" bodyone="' + e.data.bodyone + '" titletwo="' + e.data.titletwo + '" bodytwo="' + e.data.bodytwo + '" titlethree="' + e.data.titlethree + '" bodythree="' + e.data.bodythree + '" titlefour="' + e.data.titlefour + '" bodyfour="' + e.data.bodyfour + '"]');
									}
								});
							}
						}
					]
					
				},	
			]
		});
	});
})();