<?php
/**
 * The template for displaying all single portfolio items.
 *
 * @package turbulence
 */

get_header(); 

$tp_panel_color = get_theme_mod('team_single_page_member_panel_header_color');
?>
<div class="container">
	<div class="row">

	<div id="primary" class="col-lg-12">
		<main id="main" class="site-main" role="main">
        <h3><?php echo get_theme_mod('team_single_page_member_header'); ?></h3>
        <br/>
        <p><?php echo get_theme_mod('team_single_page_member_paragraph'); ?></p>
        <br/>
		<?php while ( have_posts() ) : the_post(); ?>

			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			    <div class="panel panel-<?php echo $tp_panel_color; ?>">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-12">
                                <h3 class="panel-title"><?php the_title( '<h1 class="centered">', '</h1>' ); ?></h3>
                                <h4 class="centered"><?php  echo esc_html(get_post_meta( get_the_ID(), 'job_title', true ) ); ?></h4>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="entry-content">
                    	    <div class="row">
                    	        <div class="col-md-5 col-lg-5">
                    	            <div role="tabpanel" id="infoTab">
                                        <!-- Nav tabs -->
                                        <ul class="nav nav-tabs" role="tablist">
                                            <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Email</a></li>
                                            <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Facebook</a></li>
                                            <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Twitter</a></li>
                                            <li role="presentation"><a href="#repos" aria-controls="repos" role="tab" data-toggle="tab">Git/Bitbucket</a></li>
                                        </ul>
                                        <!-- Tab panes -->
                                        <div class="tab-content">
                                            <div role="tabpanel" class="tab-pane active" id="home">
                                               <i class="fa fa-envelope fa-3x iconFix"></i>
                                               <h5 class="centered"><?php  echo esc_html(get_post_meta( get_the_ID(), 'team_member_email', true ) ); ?></h5>
                                            </div>
                                            <div role="tabpanel" class="tab-pane" id="profile">
                                                <i class="fa fa-facebook-square fa-3x iconFix"></i>
                                                <h5 class="centered"><?php  echo esc_html(get_post_meta( get_the_ID(), 'team_member_facebook', true ) ); ?></h5>
                                            </div>
                                            <div role="tabpanel" class="tab-pane" id="messages">
                                                <i class="fa fa-twitter-square fa-3x iconFix"></i>
                                                <h5 class="centered"><?php  echo esc_html(get_post_meta( get_the_ID(), 'team_member_twitter', true ) ); ?></h5>
                                            </div>
                                            <div role="tabpanel" class="tab-pane" id="repos">
                                                <i class="fa fa-git fa-3x iconFix"></i><i class="fa fa-bitbucket fa-3x iconFix"></i>
                                                <h5 class="centered"><?php  echo esc_html(get_post_meta( get_the_ID(), 'team_member_git_bitbucket', true ) ); ?></h5>
                                                
                                            </div>
                                        </div>
                                    </div>
                    	        </div>
                    		    <div class="col-md-3 col-lg-3">
                    				<?php if ( has_post_thumbnail() ) : ?>
                    					<span class="centered"><?php the_post_thumbnail('thumbnail', array('class' => 'img-rounded')); ?></span>
                    				<?php endif; ?>
                    			</div>
                    			<div class="col-md-4 col-lg-4">
                    				<?php the_content(); ?>
                    			</div>
                    		</div> <!-- .row -->
                	    </div><!-- .entry-content -->
                    </div>
                    <div class="panel-footer">
                        <div class="row">
                    	    <div class="col-md-12 col-lg-12">
                    	    Skills:
                    	    <?php
                    	       // get taxonomies terms links
                                function custom_taxonomies_terms_list(){
                                    global $post;
                                    $post = get_post($post->ID);
                                    $post_type = $post->post_type;
                                    $taxonomies = get_object_taxonomies( $post_type, 'objects' );
                                    $out = array();
                                    foreach ( $taxonomies as $taxonomy_slug => $taxonomy ){
                                    $terms = get_the_terms( $post->ID, $taxonomy_slug );
                                    if ( !empty( $terms ) ) {
                                      foreach ( $terms as $term ) {
                                        $out[] = $term->name . ", ";
                                      }
                                    }
                                  }
                                  return implode('', $out );
                                }
                                $skills = custom_taxonomies_terms_list();
                                echo $skills;
                            ?>
                    	    </div>
                    	</div>
                    </div>
                </div>
            </article><!-- #post-## -->
		<?php endwhile; // end of the loop. ?>
		</main><!-- #main -->
	</div><!-- #primary -->
    <script>
    jQuery('#infoTab a').click(function (e) {
  e.preventDefault()
  jQuery(this).tab('show')
})
</script>
</div>
</div>
<?php get_footer(); ?>