<?php
/**
 * Template Name: About Page
 *
 * This is the template that displays a contact form.
 *
 * @package turbulence
 */
 get_header(); 

 ?>
<style>
        #canvas .circle {
                display: inline-block;
                margin: 1em;
        }

        .circles-decimals {
                font-size: .4em;
        }
</style>
 <div class="container">
        <div class="row">
        	<div id="primary" class="col-lg-9 col-md-9">
        		<main id="main" class="site-main" role="main">
                      <div id="canvas">
                                <div class="circle" id="circles-1"></div>
                                <div class="circle" id="circles-2"></div>
                                <div class="circle" id="circles-3"></div>
                                <div class="circle" id="circles-4"></div>
                                <div class="circle" id="circles-5"></div>
                        </div>
        		<?php if ( have_posts() ) : ?>
        
        			<?php /* Start the Loop */ ?>
        			<?php while ( have_posts() ) : the_post(); ?>
        
        				<?php
        					/* Include the Post-Format-specific template for the content.
        					 * If you want to override this in a child theme, then include a file
        					 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
        					 */
        				get_template_part( 'content', get_post_format() );
        				?>
        
        			<?php endwhile; ?>
        
        			<?php turbulence_paging_nav(); ?>
        
        		<?php else : ?>
        
        			<?php get_template_part( 'content', 'none' ); ?>
        
        		<?php endif; ?>
        
        		</main><!-- #main -->
        	</div><!-- #primary -->

<script>
jQuery(document).ready(function($) {

        var colors = [
                        ['#D3B6C6', '#4B253A'], ['#FCE6A4', '#EFB917'], ['#BEE3F7', '#45AEEA'], ['#F8F9B6', '#D2D558'], ['#F4BCBF', '#D43A43']
                ],
                circles = [];

        for (var i = 1; i <= 5; i++) {
                var child = document.getElementById('circles-' + i),
                        percentage = 60 + (i * 10),
                        //percentage = ['HTML', 'CSS', 'PHP', 'WordPress', 'JavaScript'],
                        skills = ['HTML', 'HTML', 'HTML', 'HTML', 'HTML'],
                        circle = Circles.create({
                                id:         child.id,
                                value:      percentage,
                                //value:      skills,
                                radius:     getWidth(),
                                width:      10,
                                colors:     colors[i - 1]
                        });

                circles.push(circle);
        }

        window.onresize = function(e) {
                for (var i = 0; i < circles.length; i++) {
                        circles[i].updateRadius(getWidth());
                }
        };

        function getWidth() {
                return window.innerWidth / 20;
        }
});

</script>
<?php get_sidebar(); ?>
<?php get_footer(); ?>