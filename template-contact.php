<?php
/**
 * Template Name: Contact Page
 *
 * This is the template that displays a contact form.
 *
 * @package turbulence
 */
 
 //http://maps.googleapis.com/maps/api/staticmap?center=flowermound,texas&size=500x300&zoom=12&sensor=false
function gsm_google_static_map_shortcode(){
 
  $args = array(
    
    'city'      => get_theme_mod('google_maps_city'),
    'state'     =>get_theme_mod('google_maps_state'),   
    'zoom'      => get_theme_mod('google_maps_zoom'),
    'size'      => '600x500',
    'sensor'    => 'false',
    'markers'   => get_theme_mod('google_maps_city'),
	'contact_email' => get_theme_mod('google_maps_contact_email'),
  );
   
   
  $map_url = '<img src="http://maps.googleapis.com/maps/api/staticmap?center=';
   
  foreach($args as $arg => $value){
   
    $map_url .= $arg . '=' . urlencode($value) . '&';
   
  }
   
  $map_url .= '"/>';
   
  return $map_url;
 
}

if(isset($_POST['submitted'])) {
    if(trim($_POST['contactName']) === '') {
        $nameError = true;
        $hasError = true;
    } else {
        $name = trim($_POST['contactName']);
    }

    if(trim($_POST['email']) === '')  {
        $emailError = true;
        $hasError = true;
    } else if (!preg_match("/^[[:alnum:]][a-z0-9_.-]*@[a-z0-9.-]+\.[a-z]{2,4}$/i", trim($_POST['email']))) {
        $emailError = true;
        $hasError = true;
    } else {
        $email = trim($_POST['email']);
    }

    if(trim($_POST['comments']) === '') {
        $commentError = true;
        $hasError = true;
    } else {
        if(function_exists('stripslashes')) {
            $comments = stripslashes(trim($_POST['comments']));
        } else {
            $comments = trim($_POST['comments']);
        }
    }


    if(!isset($hasError)) {
		$emailTo = get_theme_mod('google_maps_contact_email');
        if (!isset($emailTo) || ($emailTo == '') ){
            //$emailTo = get_option('admin_email');
			$emailTo = get_theme_mod('google_maps_contact_email');
        }
        $subject = __('From ','turbulence').$name;
        $body = __('Name: ','turbulence').$name."\n".__('Email: ','turbulence').$email."\n".__('Comments: ','turbulence').$comments;
        $headers = __('From: ','turbulence') .$name. ' <'.$emailTo.'>' . "\r\n" . __('Reply-To:','turbulence') .$name. '<'.$email.'>';

        wp_mail($emailTo, $subject, $body, $headers);
        $emailSent = true;
    }

}

get_header(); ?>

<?php
 ?>

<div class="container">
<?php $gmap = get_theme_mod('contact_google_maps_enable'); ?>
<?php if($gmap == 'enable')
{
    ?>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
        		    <!-- Default panel contents -->
                    <div class="panel-heading">Contact Info</div>
                    <div class="panel-body">
                        <div class="col-md-7">
                            <?php echo gsm_google_static_map_shortcode(); ?>
                        </div>
                        <div class="col-md-5">
                            <div class="well">
                                <strong>Business Name:</strong> <?php echo get_theme_mod('google_maps_business_name'); ?><br/>
                                <strong>Address:</strong> <?php echo get_theme_mod('google_maps_address'); ?><br/>
                                <strong>City:</strong> <?php echo get_theme_mod('google_maps_city'); ?><br/>
                                <strong>State:</strong> <?php echo get_theme_mod('google_maps_state'); ?><br/>
                                <strong>Zip:</strong> <?php echo get_theme_mod('google_maps_zip'); ?><br/>
                                <strong>Phone:</strong> <?php echo get_theme_mod('google_maps_phone_number'); ?><br/>
                            </div>
                        </div>    
                    </div>
            </div>
        </div>
    </div>
<?php
}
?>
	<div class="row">
	<div id="primary" class="col-md-9 col-lg-9">
		<main id="main" class="site-main" role="main">
            
			<?php while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<header class="entry-header">
						<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
					</header><!-- .entry-header -->

					<div class="entry-content">
						<?php the_content(); ?>

				          <?php if(isset($emailSent) && $emailSent == true) { ?>
				                <div class="alert alert-success" role="alert">
				                    <p><?php _e('Thank you for subscribing, your form was submitted successfully.', 'turbulence'); ?></p>
				                </div>
				            <?php } else { ?>

				                <?php if(isset($hasError) || isset($captchaError)) { ?>
				                    <div class="alert alert-danger alert-dismissible" role="alert">
									  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
									  <strong><?php _e('Error!', 'turbulence'); ?></strong> <?php _e('Please try again.', 'turbulence'); ?>
									</div>
				                <?php } ?>

						        <form action="<?php the_permalink(); ?>" id="contactForm" method="post">
						            <div class="form-group <?php if(isset($nameError)) { echo "has-error has-feedback"; }?>">
						                    <label class="control-label" for="contactName"><?php _e('Name', 'turbulence'); ?></label>
						                    <input class="form-control" type="text" name="contactName" id="contactName" value="" />
						                    <?php if(isset($nameError)) { ?>
						                        <span class="glyphicon glyphicon-remove form-control-feedback"></span>
						                    <?php } ?>
						              
						               </div>
						               <div class="form-group <?php if(isset($emailError)) { echo "has-error has-feedback"; }?>">
						                    <label class="control-label" for="email"><?php _e('Email', 'turbulence'); ?></label>
						                
						                    <input class="form-control" type="text" name="email" id="email" value="" />
						                    <?php if(isset($emailError)) { ?>
						                        <span class="glyphicon glyphicon-remove form-control-feedback"></span>
						                    <?php } ?>
						               
						               </div>
						                <div class="form-group <?php if(isset($commentError)) { echo "has-error has-feedback"; }?>">
						                    <label class="control-label" for="commentsText"><?php _e('Message', 'turbulence'); ?></label>
						               
						                    <textarea class="form-control" name="comments" id="commentsText" rows="10" cols="20"></textarea>
						                     <?php if(isset($commentError)) { ?>
						                        <span class="glyphicon glyphicon-remove form-control-feedback"></span>
						                    <?php } ?>
						                
						               </div>
						               <div class="form-actions">
						                    <button type="submit" class="btn btn-primary"><?php _e('Send Email', 'turbulence'); ?></button>
						                    <input type="hidden" name="submitted" id="submitted" value="true" />
						               </div>
						        </form>

				        <?php } ?>
				                    
						<?php
							wp_link_pages( array(
								'before' => '<div class="page-links">' . __( 'Pages:', 'turbulence' ),
								'after'  => '</div>',
							) );
						?>
					</div><!-- .entry-content -->
					<br/>
					<footer class="entry-footer">
						<?php edit_post_link( __( 'Edit', 'turbulence' ), '<span class="edit-link">', '</span>' ); ?>
					</footer><!-- .entry-footer -->
				</article><!-- #post-## -->

			
			<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php 
if ( is_active_sidebar( 'contact' ) )
{
	get_sidebar('contact'); 
}

?>
<?php get_footer(); ?>