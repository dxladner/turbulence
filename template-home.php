<?php
/**
 * @package WordPress
 * @subpackage turbulence
 * Template Name: Homepage
 */
?>

<?php get_header(); ?>
    <section id="hero" class="light-typo">
        <div id="cover-image" class="image-bg animated fadeIn">
			 <?php if(get_theme_mod('homepage_featured_image') != '') { ?>
			    <?php $fimage = get_theme_mod('homepage_featured_image', false, 'url'); ?>
			    <img src="<?php echo $fimage; ?>" height="448" width="100%" />
			<?php } ?>
		</div>
        <div class="container welcome-content">
            <div class="middle-text">
				<?php if (get_theme_mod('homepage_featured_image_header') != '') { ?>
					<h1><?php echo get_theme_mod('homepage_featured_image_header'); ?></h1>
				<?php } ?>
             	<?php if (get_theme_mod('homepage_featured_image_subheader') != '') { ?>
					<h2><?php echo get_theme_mod('homepage_featured_image_subheader'); ?></h2>
				<?php } ?>
				
				<?php 
					$btn_text = get_theme_mod('homepage_button_text', '');
					$btn_size = 'btn-'.get_theme_mod('homepage_button_size', '');
					$btn_color = 'btn-'.get_theme_mod('homepage_button_color', '');
					$btn_url = get_theme_mod('homepage_button_url', '');
				?>	      
		    	<p><a href="<?php echo $btn_url; ?>" target="_self" class="btn <?php echo $btn_color .' '. $btn_size .' '. $btn_block; ?>" role="button"><?php echo $btn_text; ?></a></p>
            </div>
        </div>
    </section>
	<div class="container">
		<?php
		// Loop through homepage modules and get their corresponding files
		// See your theme's includes folder for editing these modules
		
		$homepage_modules = get_theme_mod('turbulence_homepage_layouts');
		
		if ($homepage_modules):
			// Loop through each module
			foreach ($homepage_modules as $key=>$value) :
				$value = preg_replace('/[0-9]+/', '', $value);
				$value = preg_replace('/\s*/', '', $value); // remove white spaces
				$value = strtolower($value); // lowercase
				get_template_part('includes/home', $value); // get correct file for each module
			endforeach;
		endif; ?>
	</div>
<?php 
 get_footer(); 
?>