<?php
/**
 * @package WordPress
 * @subpackage turbulence
 * Template Name: Landing Page
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo get_template_directory_uri(); ?>/style.css" rel="stylesheet" type="text/css">
<link href="<?php echo get_template_directory_uri(); ?>/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,700|Merriweather:400,400italic,700italic" rel="stylesheet" type="text/css">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<?php if (get_theme_mod('turbulence_favicon') != '') : ?>
	<link rel="icon" type="image/png" href="<?php echo get_theme_mod('turbulence_favicon', false, 'url'); ?>" />
<?php endif; ?>
<style>
html, body {
    background-color: <?php echo get_theme_mod('landing_page_body_background_color'); ?>; 
}
.lp-widgets {
color:<?php echo get_theme_mod('landing_page_widgets_text_color');  ?>;
}
#lp_header_content {
    background-color: <?php echo get_theme_mod('landing_page_header_background_color');  ?>;
}
.lp_header_text {
    color:<?php echo get_theme_mod('landing_page_header_text_color');  ?>;
}
.lp_subheader_text {
    color:<?php echo get_theme_mod('landing_page_subheader_text_color'); ?>;
}
.lp_text_content_paragraph p {
    color:<?php echo get_theme_mod('landing_page_paragraph_text_color');  ?>;
}
</style>
</head>

<body <?php body_class(); ?>>
<div class="wrapper"><!-- start wrapper -->

       
            <div id="lp_header_content">
                <div class="row">
                    <div class="col-md-3">
                    </div>
                    <div class="col-md-6">
                        <h1 class="centeredText lp_header_text">
                            <?php if (get_theme_mod('landing_page_main_header') != '') { ?>
                    			<?php echo get_theme_mod('landing_page_main_header'); ?>
                    		<?php } ?>
                    	</h1>
                    </div>
                    <div class="col-md-3">
                    </div>
                </div>
                <div class="row">
                	<div class="col-md-3">
                    </div>
                    <div class="col-md-6">
                    	<h4 class="centeredText lp_subheader_text">
                    	    <?php if (get_theme_mod('landing_page_main_subheader') != '') { ?>
                    			<?php echo get_theme_mod('landing_page_main_subheader'); ?>
                    		<?php } ?>
                    	</h4>
                    </div>
                    <div class="col-md-3">
                    </div>
                </div>
            </div>
            <div id="lp_forms" class="container">
            		<?php
            		// Loop through homepage modules and get their corresponding files
            		// See your theme's includes folder for editing these modules
            		$landingpage_modules = get_theme_mod('turbulence_landing_page_layouts');
            		if ($landingpage_modules):
            			// Loop through each module
            			foreach ($landingpage_modules as $key=>$value) :
                            $value = preg_replace('/[0-9]+/', '', $value);
            				$value = preg_replace('/\s*/', '', $value); // remove white spaces
            				$value = strtolower($value); // lowercase
            				get_template_part('includes/landingpage', $value); // get correct file for each module
            			endforeach;
            		endif; ?>
            </div>
            <br/>
            <br/>
            <div id="lp_main_content" class="container">
                <?php if(get_theme_mod('landing_page_featured_image') != '') { ?>
        		    <?php $lpimage = get_theme_mod('landing_page_featured_image', false, 'url'); ?>
        			    <img src="<?php echo $lpimage; ?>" height="448" width="100%" />
        		<?php } ?>
        	</div>
    		<br/>
    		<br/>
    		<?php 
            $landing_page_paragraph_switch = get_theme_mod('landing_page_paragraph_enable');
            if($landing_page_paragraph_switch == 'enable') 
            {
            ?>
            	<div id="lp_text_content_paragraph" class="container">
            	<p>
            	    <?php if (get_theme_mod('landing_page_paragraph_content') != '') { ?>
                		<?php echo get_theme_mod('landing_page_paragraph_content'); ?>
                	<?php } ?>
            	</p>
                </div>
            <?php
            }
            ?>
            <br/>
            <br/>
            <br/>
            <div class="lp-widgets container">
            	<div class="row">
            		<div class="col-lg-4 col-md-4">
            	    <?php $fa_icon_one = get_theme_mod('landing_page_widgets_one_icon', ''); ?>
            		    
            			<p class="widgets_icon"><i class="fa fa-<?php echo $fa_icon_one; ?> fa-4x"></i></p>
            			<h4 class="widgets_header">
            			<?php if (get_theme_mod('landing_page_widget_one_header') != '') { ?>
            				<?php echo get_theme_mod('landing_page_widget_one_header'); ?>
            			<?php } ?>
            			</h4>
            			<p class="widgets_content">
            			<?php if (get_theme_mod('landing_page_widget_one_content') != '') { ?>
            				<?php echo get_theme_mod('landing_page_widget_one_content'); ?>
            			<?php } ?>
            			</p>
            		</div>
            
            		<div class="col-lg-4 col-md-4">
            			<?php $fa_icon_two = get_theme_mod('landing_page_widgets_two_icon', ''); ?>
            		    
            			<p class="widgets_icon"><i class="fa fa-<?php echo $fa_icon_two; ?> fa-4x"></i></p>
            			<h4 class="widgets_header">
            			<?php if (get_theme_mod('landing_page_widget_two_header') != '') { ?>
            				<?php echo get_theme_mod('landing_page_widget_two_header'); ?>
            			<?php } ?>
            			</h4>
            			<p class="widgets_content">
            			<?php if (get_theme_mod('landing_page_widget_two_content') != '') { ?>
            				<?php echo get_theme_mod('landing_page_widget_two_content'); ?>
            			<?php } ?>
            			</p>
            		</div>
            
            		<div class="col-lg-4 col-md-4">
            			<?php $fa_icon_three = get_theme_mod('landing_page_widgets_three_icon', ''); ?>
            		    
            			<p class="widgets_icon"><i class="fa fa-<?php echo $fa_icon_three; ?> fa-4x"></i></p>
            			<h4 class="widgets_header">
            			<?php if (get_theme_mod('landing_page_widget_three_header') != '') { ?>
            				<?php echo get_theme_mod('landing_page_widget_three_header'); ?>
            			<?php } ?>
            			</h4>
            			<p class="widgets_content"><?php if (get_theme_mod('landing_page_widget_three_content') != '') { ?>
            				<?php echo get_theme_mod('landing_page_widget_three_content'); ?>
            			<?php } ?>
            			</p>
            		</div>
            
            	</div><!--row-->
            </div>
            <br/>
            <br/>
            <br/>
            <?php
                $lp_home_btn_text = get_theme_mod('landing_page_home_page_button_text');
            	$lp_home_btn_size = 'btn-'.get_theme_mod('landing_page_home_page_button_size');
            	$lp_home_btn_color = 'btn-'.get_theme_mod('landing_page_home_page_button_color');
                $lp_home_btn_url = get_theme_mod('landing_page_home_page_button_url');
                
                $landing_page_home_button_switch = get_theme_mod('landing_page_home_page_button_enable');
                if($landing_page_home_button_switch == 'enable') 
                {
            ?>
                <div id="lp_home_page_button">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="centeredText">
		    	            <p><a href="<?php echo $lp_home_btn_url; ?>" target="_self" class="btn <?php echo $lp_home_btn_color .' '. $lp_home_btn_size; ?>" role="button"><?php echo $lp_home_btn_text; ?></a></p>
                            </div>
                        </div>
                    </div>
                </div>
            <?php
            }
            ?>
            
            <?php 
            $landing_page_footer_switch = get_theme_mod('landing_page_footer_enable');
            if($landing_page_footer_switch == 'enable') 
            {
            ?>
                <div id="lp_footer_content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="social-icons centeredText">
                                        <?php
                                        $social_options = array(); 
                                        if (get_theme_mod('social_media_icon_one') != '')
                                        {
                                            $social_media_icon_one = get_theme_mod('social_media_icon_one');
                                            $social_media_one_user_id = get_theme_mod('social_media_one_user_id');
                                            array_push($social_options, $social_media_icon_one, $social_media_one_user_id);
                                        }

                                        if (get_theme_mod('social_media_icon_two') != '')
                                        {
                                            $social_media_icon_two = get_theme_mod('social_media_icon_two');
                                            $social_media_two_user_id = get_theme_mod('social_media_two_user_id');
                                            array_push($social_options, $social_media_icon_two, $social_media_two_user_id);
                                        }

                                        if (get_theme_mod('social_media_icon_three') != '')
                                        {
                                            $social_media_icon_three = get_theme_mod('social_media_icon_three');
                                            $social_media_three_user_id = get_theme_mod('social_media_three_user_id');
                                            array_push($social_options, $social_media_icon_three, $social_media_three_user_id);
                                        }

                                        if (get_theme_mod('social_media_icon_four') != '')
                                        {
                                            $social_media_icon_four = get_theme_mod('social_media_icon_four');
                                            $social_media_four_user_id = get_theme_mod('social_media_four_user_id');
                                            array_push($social_options, $social_media_icon_four, $social_media_four_user_id);
                                        }

                                        if (get_theme_mod('social_media_icon_five') != '')
                                        {
                                            $social_media_icon_five = get_theme_mod('social_media_icon_five');
                                            $social_media_five_user_id = get_theme_mod('social_media_five_user_id');
                                            array_push($social_options, $social_media_icon_five, $social_media_five_user_id);
                                        }

                                        if (get_theme_mod('social_media_icon_six') != '')
                                        {
                                            $social_media_icon_six = get_theme_mod('social_media_icon_six');
                                            $social_media_six_user_id = get_theme_mod('social_media_six_user_id');
                                            array_push($social_options, $social_media_icon_six, $social_media_six_user_id);
                                        }

                                        if (get_theme_mod('social_media_icon_seven') != '')
                                        {
                                            $social_media_icon_seven = get_theme_mod('social_media_icon_seven');
                                            $social_media_seven_user_id = get_theme_mod('social_media_seven_user_id');
                                            array_push($social_options, $social_media_icon_seven, $social_media_seven_user_id);
                                        }

                                        if (get_theme_mod('social_media_icon_eight') != '')
                                        {
                                            $social_media_icon_eight = get_theme_mod('social_media_icon_eight');
                                            $social_media_eight_user_id = get_theme_mod('social_media_eight_user_id');
                                            array_push($social_options, $social_media_icon_eight, $social_media_eight_user_id);
                                        }

                                        if (get_theme_mod('social_media_icon_nine') != '')
                                        {
                                            $social_media_icon_nine = get_theme_mod('social_media_icon_nine');
                                            $social_media_nine_user_id = get_theme_mod('social_media_nine_user_id');
                                            array_push($social_options, $social_media_icon_nine, $social_media_nine_user_id);
                                        }

                                        if (get_theme_mod('social_media_icon_ten') != '')
                                        {
                                            $social_media_icon_ten = get_theme_mod('social_media_icon_ten');
                                            $social_media_ten_user_id = get_theme_mod('social_media_ten_user_id');
                                            array_push($social_options, $social_media_icon_ten, $social_media_ten_user_id);
                                        }
                                        
                                        foreach ( $social_options as $key => $value ) 
                                        {
                                            if ( $value ) 
                                            { 
                                            ?>
                                                <a href="<?php echo $value; ?>" title="<?php echo $key; ?>" target="_blank">
                                                    <i class="fa fa-<?php echo $value; ?>"></i>
                                                </a>
                                            <?php 
                                            }
                                        } 
                                        ?>
                                </div><!-- .social-icons -->
                        </div>
                    </div>
                </div>
            <?php
            }
            ?>

</div><!-- end wrapper -->
</body>
</html>