<?php
/**
 * Template Name: Services
 *
 */

get_header(); ?>
<style>
.sp_call_to_action {
    background-color: <?php echo get_theme_mod('services_page_call_to_action_header_background_color');  ?>;
    color:<?php echo get_theme_mod('services_page_call_to_action_header_text_color');  ?>;
}
.services {
    background-color: <?php echo get_theme_mod('services_page_background_color');  ?>;
    color:<?php echo get_theme_mod('services_page_text_color');  ?>;
}
</style>
    <?php 
    $services_enable = get_theme_mod( 'services_call_to_section_enable' );

    if ($services_enable == 'enable')
    {
    ?>
	<div class="row">
	    <div class="sp_call_to_action">
	        <h2 class="centered paddingTop">
	        <?php if (get_theme_mod('services_page_call_to_action_header') != '') { ?>
                <?php echo get_theme_mod('services_page_call_to_action_header'); ?>
            <?php } ?>
	        </h2>
	        <h4 class="centered">
	        <?php if (get_theme_mod('services_page_call_to_action_subheader') != '') { ?>
                <?php echo get_theme_mod('services_page_call_to_action_subheader'); ?>
            <?php } ?>
	        </h4>
	        <h4 class="centered">
	        <?php if (get_theme_mod('services_page_call_to_action_phone_number') != '') { ?>
                <?php echo get_theme_mod('services_page_call_to_action_phone_number'); ?>
            <?php } ?>
	        </h4>
	        <h4 class="centered">
	        <?php if (get_theme_mod('services_page_call_to_action_email') != '') { ?>
                <?php echo get_theme_mod('services_page_call_to_action_email'); ?>
            <?php } ?>
	        </h4>
	    </div>
	</div>
    <?php 
    }
    ?>
    <div class="container services">
	    <div class="row">	
           
					<div class="col-sm-4 col-md-4">
					     <?php $sp_icon_one = get_theme_mod('services_widgets_one_icon', ''); ?>
            		    
            			<p class="widgets_icon"><i class="fa fa-<?php echo $sp_icon_one; ?> fa-4x"></i></p>
            			<h4 class="widgets_header">
            			<?php if (get_theme_mod('services_widget_one_header') != '') { ?>
            				<?php echo get_theme_mod('services_widget_one_header'); ?>
            			<?php } ?>
            			</h4>
            			<p class="widgets_content">
            			<?php if (get_theme_mod('services_widget_one_content') != '') { ?>
            				<?php echo get_theme_mod('services_widget_one_content'); ?>
            			<?php } ?>
            			</p>
					</div>
					
					<div class="col-sm-4 col-md-4">
					    <?php $sp_icon_two = get_theme_mod('services_widgets_two_icon', ''); ?>
            		    
            			<p class="widgets_icon"><i class="fa fa-<?php echo $sp_icon_two; ?> fa-4x"></i></p>
            			<h4 class="widgets_header">
            			<?php if (get_theme_mod('services_widget_two_header') != '') { ?>
            				<?php echo get_theme_mod('services_widget_two_header'); ?>
            			<?php } ?>
            			</h4>
            			<p class="widgets_content">
            			<?php if (get_theme_mod('services_widget_two_content') != '') { ?>
            				<?php echo get_theme_mod('services_widget_two_content'); ?>
            			<?php } ?>
            			</p>
					</div>
					
					<div class="col-sm-4 col-md-4">
					    <?php $sp_icon_three = get_theme_mod('services_widgets_three_icon', ''); ?>
            		    
            			<p class="widgets_icon"><i class="fa fa-<?php echo $sp_icon_three; ?> fa-4x"></i></p>
            			<h4 class="widgets_header">
            			<?php if (get_theme_mod('services_widget_three_header') != '') { ?>
            				<?php echo get_theme_mod('services_widget_three_header'); ?>
            			<?php } ?>
            			</h4>
            			<p class="widgets_content"><?php if (get_theme_mod('services_widget_three_content') != '') { ?>
            				<?php echo get_theme_mod('services_widget_three_content'); ?>
            			<?php } ?>
            			</p>
					</div>
        </div><!-- end row -->
        <?php 
            $add_services_row_switch = get_theme_mod('services_add_new_row_enable');
            if($add_services_row_switch == 'enable') 
            {
            ?>
        <div class="row">    
                	<div class="col-sm-4 col-md-4">
					     <?php $sp_icon_four = get_theme_mod('services_widgets_four_icon', ''); ?>
            		    
            			<p class="widgets_icon"><i class="fa fa-<?php echo $sp_icon_four; ?> fa-4x"></i></p>
            			<h4 class="widgets_header">
            			<?php if (get_theme_mod('services_widget_four_header') != '') { ?>
            				<?php echo get_theme_mod('services_widget_four_header'); ?>
            			<?php } ?>
            			</h4>
            			<p class="widgets_content">
            			<?php if (get_theme_mod('services_widget_four_content') != '') { ?>
            				<?php echo get_theme_mod('services_widget_four_content'); ?>
            			<?php } ?>
            			</p>
					</div>
					
					<div class="col-sm-4 col-md-4">
					    <?php $sp_icon_five = get_theme_mod('services_widgets_five_icon', ''); ?>
            		    
            			<p class="widgets_icon"><i class="fa fa-<?php echo $sp_icon_five; ?> fa-4x"></i></p>
            			<h4 class="widgets_header">
            			<?php if (get_theme_mod('services_widget_five_header') != '') { ?>
            				<?php echo get_theme_mod('services_widget_five_header'); ?>
            			<?php } ?>
            			</h4>
            			<p class="widgets_content">
            			<?php if (get_theme_mod('services_widget_five_content') != '') { ?>
            				<?php echo get_theme_mod('services_widget_five_content'); ?>
            			<?php } ?>
            			</p>
					</div>
					
					<div class="col-sm-4 col-md-4">
					    <?php $sp_icon_six = get_theme_mod('services_widgets_six_icon', ''); ?>
            		    
            			<p class="widgets_icon"><i class="fa fa-<?php echo $sp_icon_six; ?> fa-4x"></i></p>
            			<h4 class="widgets_header">
            			<?php if (get_theme_mod('services_widget_six_header') != '') { ?>
            				<?php echo get_theme_mod('services_widget_six_header'); ?>
            			<?php } ?>
            			</h4>
            			<p class="widgets_content"><?php if (get_theme_mod('services_widget_six_content') != '') { ?>
            				<?php echo get_theme_mod('services_widget_six_content'); ?>
            			<?php } ?>
            			</p>
					</div>
            
            <?php
            }
            ?>
        </div><!-- end row -->
    </div>
    <br/>
    <br/>
<?php get_footer(); ?>