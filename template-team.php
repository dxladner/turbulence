<?php
/**
 * Template Name: Team
 *
 */

get_header(); ?>
<style>
html, body {
    background-color: <?php echo get_theme_mod('team_page_body_background_color'); ?>; 
}
.team {
    background-color: <?php echo get_theme_mod('team_page_images_background_color'); ?>;
}
.tp_header {
    color: <?php echo get_theme_mod('team_page_header_text_color'); ?>;
}
.site-main a {
    color: <?php echo get_theme_mod('team_page_member_name_text_color'); ?>;
}
.job_title {
    color: <?php echo get_theme_mod('team_page_member_job_title_text_color'); ?>;
}
</style>
<div class="container team">
	<div class="row">

	<div id="primary" class="col-md-12 col-lg-12">
		<main id="main" class="site-main" role="main">
			<h1 class="centered tp_header">Team Members Page</h1>
            <?php 
			// Portfolio columns variable from Theme Options
			$pcount = get_theme_mod('team_page_columns'); ?>
			<?php 
			// the query
			$the_query = new WP_Query( array('post_type' => 'team') ); ?>

			<?php if ( $the_query->have_posts() ) : ?>

				<div class="row">
					<div id="team-items">

					<!-- the loop -->
					<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

					  <?php
		                $terms = get_the_terms( $post->ID, 'skills' );
		                                     
		                if ( $terms && ! is_wp_error( $terms ) ) : 
		                    $links = array();
		 
		                    foreach ( $terms as $term ) 
		                    {
		                        $links[] = $term->name;
		                    }
		                    $links = str_replace(' ', '-', $links); 
		                    $tax = join( " ", $links );     
		                else :  
		                    $tax = '';  
		                endif;
		                ?>

					<div class="col-sm-6 col-md-<?php echo $pcount; ?> item <?php echo strtolower($tax); ?>">
						<div class="team-item">
							<a class="thumbnail" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
						    <?php the_post_thumbnail('thumbnail', array('class' => 'img-circle')); ?>
							</a>
							<h4 class="centeredTextBlack"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
							<h5 class="centered job_title"><?php  echo esc_html(get_post_meta( get_the_ID(), 'job_title', true ) ); ?></h5>
						</div>
					</div>
					<?php endwhile; ?>
					<!-- end of the loop -->

				</div> <!-- #portfolio-items -->

				</div> <!-- .row -->
				

				<?php wp_reset_postdata(); ?>

			<?php else : ?>
				<p><?php _e( 'Sorry, no Team Members matched your criteria.', 'turbulence' ); ?></p>
			<?php endif; ?>

			
		</main><!-- #main -->
	</div><!-- #primary -->

	</div><!-- .row -->
</div><!-- .container -->
<br/>
<br/>
<?php get_footer(); ?>